package com.tis.TIS_NextGen;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.annotations.Test;

import com.tis.webdriver.pagefactory.pageobject.notifications.NotificationsListPage;

public class Test3 {
	@Test(enabled = false)
	public void verifyMethod() throws ParseException
	{
		NotificationsListPage.yearMonth();
		NotificationsListPage.getTheDate();
	}
	@Test
	public void test12() throws ParseException
	{
		String sDate1="31 Dec 1998";
		Date date1=new SimpleDateFormat("dd MMM yyyy").parse(sDate1);
		System.out.println(sDate1+"\t"+date1);
	}

}

package com.tis.TIS_NextGen;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class TestClass {
	public static RemoteWebDriver createInstance(String ExecutionSystem,String URL) throws MalformedURLException 
	{
		
		/* Invoking internert Explorer browser start
		 * 
		*/
		
		//String URL =  "http://demo-motion2-20170619.s3-website.ap-south-1.amazonaws.com";
		RemoteWebDriver driver = null;
		
		String  msrtMachineAddress="127.0.0.1";
		
	//	String URL =  "https://www.google.co.in";
		String url="http://"+msrtMachineAddress+":5547/wd/hub";
		
	
		url="http://"+msrtMachineAddress+":5547/wd/hub";
    	DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
        capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);           
        capabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, URL);
    	System.setProperty("webdriver.ie.driver","C:\\Selenium\\IEDriverServer_Win32_3.4.0\\IEDriverServer.exe");
    	//driver=new InternetExplorerDriver(capabilities);
    	driver=new RemoteWebDriver(new URL(url), capabilities);
    	driver.get(URL);
    	
		return driver;
		
	}
}

package com.tis.TIS_NextGen;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

public class TestIE {

	WebDriver driver;
	//DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
  //  capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true); 
	@Test
	
	public void testIE() throws InterruptedException
	{
		System.setProperty("webdriver.ie.driver","C:\\Selenium\\IEdriver\\IEDriverServer_x64_3.1.0\\IEDriverServer.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();

		capabilities.setCapability(CapabilityType.BROWSER_NAME, "IE");

		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);

		capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
		capabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "http://demo-motion2-20170619.s3-website.ap-south-1.amazonaws.com");

		//System.setProperty("webdriver.ie.driver","C://MavenTest//driver//IEDriverServer.exe");
		
		driver = new InternetExplorerDriver(capabilities);
		
		driver.get("http://demo-motion2-20170619.s3-website.ap-south-1.amazonaws.com");
		Thread.sleep(10000);
		driver.findElement(By.xpath(".//*[@id='planner-tab']/a/span")).click();
		
		//driver.findElement(By.cssSelector("#lst-ib")).sendKeys(Keys.ENTER);
	}
}

package com.tis.webdriver.testcases.plannertests;

import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.planner.CurrentStatus_PlannerPage;
import com.tis.webdriver.pagefactory.pageobject.planner.PlannerPage;


public class Trace_SearchTest extends NewTestTemplate
{
	By sliderBtn = By.cssSelector("img[alt='sliderbutton']");
	
	@Test
	public void currentStatus_001_SerchDriverInAsset(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Search Driver");
		new TisHomePage(driver).clickOnPlanner();
		PlannerPage currentStatus=new PlannerPage(driver);
		Thread.sleep(5000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBtn);
		Thread.sleep(1000);
		PlannerPage.clickOnSearch();
		Thread.sleep(5000);
		String dName=CurrentStatus_PlannerPage.getDriverName();
		currentStatus.searchDriverVehicleOnMap(dName);
		//currentStatus.searchDriverVehicleOnMap("ARNAUD EMMANUEL");
		Thread.sleep(2000);
		CurrentStatus_PlannerPage.veriFyTheSearch(dName);
	}
	@Test(enabled = false)
	public void currentStatus_002_SearchVehicleInAsset(Method method) throws InterruptedException, IOException
	{
		test=createTest(method.getName(), "Search Vehicle in Asset");
		
		driver.findElement(By.cssSelector("#menu-button>a>img")).click();
		Thread.sleep(5000);
		new TisHomePage(driver).clickOnPlanner();
		Thread.sleep(2000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBtn);
		
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("li[class='text-uppercase list-inline-item activateable section-vehicles section-inactive']")).click();

		PlannerPage currentStatus=new PlannerPage(driver);
		Thread.sleep(2000);
		PlannerPage.clickOnSearch();
		Thread.sleep(2000);
		
		String vName=CurrentStatus_PlannerPage.getVehicleName();
		Thread.sleep(2000);
		currentStatus.searchDriverVehicleOnMap(vName);
		//currentStatus.searchDriverVehicleOnMap("ARNAUD EMMANUEL");
		Thread.sleep(2000);
		CurrentStatus_PlannerPage.veriFyTheSearch(vName);
	}
	@Test(enabled = false)
	public void currentStatus_003_VerifySearchSuggestions(Method method) throws InterruptedException, IOException
	{
		test = createTest(method.getName(), "Verify the search suggestions");
		new TisHomePage(driver).clickOnPlanner();
		Thread.sleep(5000);
		PlannerPage.clickOnSearch();
		new PlannerPage(driver).searchSuggestions("test");
	}
}

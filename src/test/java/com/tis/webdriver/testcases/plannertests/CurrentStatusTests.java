package com.tis.webdriver.testcases.plannertests;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.configuration.Configuration;
import com.tis.webdriver.common.configuration.Credentials;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.planner.CurrentStatus_PlannerPage;
import com.tis.webdriver.pagefactory.pageobject.planner.PlannerPage;
import com.tis.webdriver.pagefactory.pageobject.planner.Settings_PlannerPage;

public class CurrentStatusTests extends NewTestTemplate
{
	Credentials credentials = Configuration.getCredentials();

	
	/*
	 * Verify the Planner page loaded
	 * */
	
	@Test
	public void currentStatus_001_VerifyPageHeader(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the page header of the PlannerDefault Page");
		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(5000);
		homepage.clickOnPlanner();
		Thread.sleep(5000);
		PlannerPage.verifyPageHeaderPlanner();
		
	}

	@Test
	public void currentStatus_002_VerifyDriverInAssets(Method method) throws InterruptedException, IOException
	{
		test=createTest(method.getName(), "Verify the driver in list present in Assets");
		new TisHomePage(driver).clickOnPlanner();
		
		PlannerPage.clickOnOpenAsset();
	
		Thread.sleep(3000);
		//Index 2 has been passed in the below method to select the driver from the list
		WebElement element=PlannerPage.totalDriversInDropDown().get(2);
		String dName = element.getText();
		element.click();
		CurrentStatus_PlannerPage.veriFyTheSearch(dName);
		
	}
	@Test
	public void currentStatus_003_VerifyVehicleInAssets(Method method) throws InterruptedException, IOException
	{
		test=createTest(method.getName(), "Verify the driver in list present in Assets");
		new TisHomePage(driver).clickOnPlanner();
		Thread.sleep(1000);
		driver.findElement(By.cssSelector("li[class='text-uppercase list-inline-item activateable section-vehicles section-inactive']")).click();
		Thread.sleep(3000);
		
		//Index 2 has been passed in the below method to select the driver from the list
		WebElement element=PlannerPage.verifyAvailableVehiclesInDropMenu().get(2);
		String vName = element.getText();
		element.click();
		CurrentStatus_PlannerPage.veriFyTheSearch(vName);
		PlannerPage.clickOnAssetCloseImg();
		}
	
	@Test
	public void currentStatus_004_VerifyTheSiteInTheMap(Method method) throws InterruptedException, IOException
	{
		test=createTest(method.getName(), "Verify the selected Site displayed in the Map");

		PlannerPage.verifySiteInTheMap();
	}
	@Test
	public void currentStatus_005_verifyVehicleCapacitySlider(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the slider Movement");
		PlannerPage.clickOnOpenAsset();
		//new CurrentStatus_PlannerPage().clickOnMapView();
		CurrentStatus_PlannerPage.moveVehicleSliderTo80();
		//CurrentStatus_PlannerPage.verifyTheVehicleCapacityOfTheAvailableVehilces();
	}

}

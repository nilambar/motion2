package com.tis.webdriver.testcases.plannertests;

import java.io.IOException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.tis.webdriver.common.configuration.Configuration;
import com.tis.webdriver.common.configuration.Credentials;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.loginarea.LoginArea;
import com.tis.webdriver.pagefactory.pageobject.planner.CurrentStatus_PlannerPage;
import com.tis.webdriver.pagefactory.pageobject.planner.PlannerPage;
import com.tis.webdriver.pagefactory.pageobject.planner.Trace_PlannerPage;
import com.tis.webdriver.testcases.logintests.LoginTests;


public class TraceTests extends NewTestTemplate
{
	By sliderBttn = By.cssSelector("#SliderbuttonBtn");
	//By driverName = By.cssSelector("span[class='mr-auto txt-overflow']");
	By slideShiftDetail= By.cssSelector("span[class='text-vdo-blue mr-20px']");
	
	@BeforeTest
	public void setUP() throws InterruptedException, IOException
	{
		//test = ExtentManager.createTest(method.getName(), "Verify the slider button opened on the page");
 	    //CurrentStatus_PlannerPage currentStatusPlannerPage = new CurrentStatus_PlannerPage();
		TisHomePage homepage = new TisHomePage(driver);
		//TisBasePageObject base = new TisBasePageObject();
		//base.openTisPage(tisURL);
		Thread.sleep(10000);
		homepage.clickOnPlanner();
		Thread.sleep(20000);
		new PlannerPage(driver).openTraceTab();
		Thread.sleep(2000);
	
	}
	@Test (enabled= false) 
	public void plannerTrace_001_loadTracePage(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the slider button opened on the page");
		//CurrentStatus_PlannerPage currentStatusPlannerPage = new CurrentStatus_PlannerPage();
		TisHomePage homepage = new TisHomePage(driver);
		//TisBasePageObject base = new TisBasePageObject();
		//base.openTisPage(tisURL);
		Thread.sleep(5000);
		homepage.clickOnPlanner();
		Thread.sleep(20000);
		new PlannerPage(driver).openTraceTab();
	
	}
	@Test
	public void plannerTrace_002_VerifyDefaultMapLoad(Method method) throws IOException, InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the default Map View Page");
		Thread.sleep(10000);
		new Trace_PlannerPage(driver).veriFyDefatulMainMapView();
		new Trace_PlannerPage(driver).getTheDefaultDate();
	}
	@Test
	public void plannerTrace_003_VerifyAvailableSitesInDropDown(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the available Sites in the dropdown");
		Trace_PlannerPage.verifyAvailableSitesInDropMenu();
	}
	@Test 
	public void plannerTrace_004_VerifyAvailableDriversInDropDown(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the available Drivers in the dropdown");
		Trace_PlannerPage.totalDriversInDropDown();
	}
	@Test
	public void plannerTrace_005_VerifyAvailableVehiclesInDropDownMenu(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the available Vehicles in the dropdown");
		Trace_PlannerPage.verifyAvailableVehiclesInDropMenu();
	}
	@Test
	public void plannerTrace_006__VerifyToggle(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Click on Toggle to verify the asset");
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		Trace_PlannerPage.getAllDriversInSliders();
	}
	@Test 
	public void plannerTrace_007_VerifyTheDriverSelection(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify The Trace for the Vehicle");

		Trace_PlannerPage.selectDriver(2);

	}
	@Test 
	public void plannerTrace_008_DateVerification(Method method) throws InterruptedException, ParseException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the Date in Trace, it should be the previous day");
		Trace_PlannerPage.getTheDateRange();
	

	
	}
	@Test
	public void plannerTrace_009_VeriFyShiftDetails(Method method)
	{
		test = ExtentManager.createTest(method.getName(), "Verify The Shift Details");
		Trace_PlannerPage.clickShiftDetail();
		Trace_PlannerPage.verifyShiftDetails();
	}
	@Test
	public void plannerTrace_010_VerifyHideAsset(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify The Hide Asset Button");
		Trace_PlannerPage.clickOnMapView();
		Thread.sleep(4000);
		Trace_PlannerPage.clickOnAssetHideBttn(sliderBttn);
		Thread.sleep(2000);
		Trace_PlannerPage.verifyIsAssetHide();
		
	}
	@Test
	public void plannerTrace_011_VerifyTrace(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify The Trace for the Vehicle");
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		Thread.sleep(2000);
		Trace_PlannerPage.verifyTheTrace(0);
		Thread.sleep(2000);
		Trace_PlannerPage.clickOnCloseSliderBttn();

	}
	@Test
	public void plannerTrace_012_VerifyTheDriverSelection(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify The Trace for the Vehicle");

		
		Trace_PlannerPage.selectDriver(2); //Select second driver from the list
		
	}
	@Test
	public void plannerTrace_013_VerifyTheVehicleSelection(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify The Trace for the Vehicle");
		
		Trace_PlannerPage.selectVehicle(0); //Select first vehicle from the list
		
	}
	@Test
	public void plannerTrace_014_VerifyTheSelectedVehicleAvailabeInAsset(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify The Vehicle Available in the Asset");
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		Thread.sleep(2000);
		Trace_PlannerPage.clickOnVehiclesTabInAsset();
		Thread.sleep(1000);
		Trace_PlannerPage.verifyVehicleInAsset(1);
		Trace_PlannerPage.clickOnCloseSliderBttn();

	}
	@Test
	public void plannerTrace_015_VerifySearch(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify Search");
		Thread.sleep(4000);
		Trace_PlannerPage.clickOnSearch();
		Thread.sleep(4000);
		Trace_PlannerPage.enterSearchText();
		Thread.sleep(4000);
		Trace_PlannerPage.gettingSearchValue();
	}
	@Test
	public void plannerTrace_016_VerifyTheDatePicker(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify The DatePicker");
		Thread.sleep(3000);
		Trace_PlannerPage.clickOnDateTextBox();
		Trace_PlannerPage.cancelCalendar();
	}

}

package com.tis.webdriver.testcases.plannertests;

import java.io.IOException;
import java.lang.reflect.Method;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.configuration.Configuration;
import com.tis.webdriver.common.configuration.Credentials;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.planner.CurrentStatus_PlannerPage;
import com.tis.webdriver.pagefactory.pageobject.planner.PlannerPage;
import com.tis.webdriver.pagefactory.pageobject.planner.Settings_PlannerPage;

public class CurrentStatus_SettingPageTests extends NewTestTemplate
{
	Credentials credentials = Configuration.getCredentials();

		
	@Test(enabled = false)
	public void currentStatus_001_VerifySettingTab(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify PlannerSetting Page");
		//TisBasePageObject base = new TisBasePageObject();
		//base.openTisPage(tisURL);
		Thread.sleep(5000);
		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(5000);
		homepage.clickOnPlanner();
		PlannerPage.openPlannerSettings();
		Thread.sleep(2000);
		Settings_PlannerPage.verifyAllSiteCheckboxChecked();
	}
	
	@Test(enabled = false)
	public void currentStatus_002_VerifySitesRadioButton(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify PlannerSetting Page");
		Thread.sleep(2000);
		Settings_PlannerPage.verifySitesCheckBox();
	
	}
	
	@Test(enabled = false)
	public void currentStatus_003_VerifySavedSite(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify PlannerSetting Page");
		Thread.sleep(2000);
		Settings_PlannerPage.saveSitesData("Base1");
		
	}
	
	@Test(enabled = false)
	public void currentStatus_008_VerifySiteUpdateFromSetting(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the site under current status");
		PlannerPage.openPlannerSettings();
		Settings_PlannerPage.clickSiteRadio();
		Settings_PlannerPage.saveSitesData("Base1");
		new TisHomePage(driver).clickOnPlanner();
		Thread.sleep(3000);
		new CurrentStatus_PlannerPage().verifyUpdatedSiteInCurrentStatusPage("Base1");
	
	}
	
	
	
		
}

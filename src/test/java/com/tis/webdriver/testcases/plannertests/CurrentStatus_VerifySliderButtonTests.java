/**
 * This Class is used to verify the Current Status Tests in the Application
 */

package com.tis.webdriver.testcases.plannertests;

import java.awt.AWTException;
import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.planner.CurrentStatus_PlannerPage;
import com.tis.webdriver.pagefactory.pageobject.planner.PlannerPage;
import com.tis.webdriver.pagefactory.pageobject.planner.Settings_PlannerPage;

public class CurrentStatus_VerifySliderButtonTests extends NewTestTemplate
{
	By sliderBttn = By.cssSelector("#btn-opener>img");
	
	@BeforeTest
	public void currentStatusClickOnPlanner() throws InterruptedException
	{
		
		TisHomePage homepage = new TisHomePage(driver);
	
		Thread.sleep(10000);
		homepage.clickOnPlanner();
	}
	
	@Test  
	public void currentStatus_001_VerifyRemainingDrivingDefaultValue(Method method)
	{
		test = ExtentManager.createTest(method.getName(), "Verify the default value displayed for remaining driving time");
		CurrentStatus_PlannerPage currentStatusPlannerPage = new CurrentStatus_PlannerPage();
		currentStatusPlannerPage.defaultValueForRemainingDrivingTime();
	}
	
	/**
	 * currentStatus_001_verifySliderButton() :
	 * Test to Verify in the Current Status screen Asset Button clicked and Drivers available in the slider
	 * @param method
	 * @throws InterruptedException
	 * @throws IOException
	 */
	
	
	
	
	@Test 
	public void currentStatus_002_verifySliderButton(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the slider button opened on the page");
		CurrentStatus_PlannerPage currentStatusPlannerPage = new CurrentStatus_PlannerPage();
		Thread.sleep(5000);
		
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		Thread.sleep(2000);
		currentStatusPlannerPage.verifySliderButton();
		Thread.sleep(2000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
	}
	
	/**
	 * Test to Verify in the Current Status screen full view Mode in the asset
	 * @param method
	 * @throws IOException
	 * @throws InterruptedException
	 */
	
	@Test 
	public void currentStatus_003_VerifyFullViewMode(Method method) throws IOException, InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "On clicking Full View ,verify Full View List displayed");
		CurrentStatus_PlannerPage currentStatusPlannerPage = new CurrentStatus_PlannerPage();
		Thread.sleep(4000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		
		currentStatusPlannerPage.clickOnFullViewList();
		Thread.sleep(1000);
		currentStatusPlannerPage.verifyfullViewMode();
		Thread.sleep(1000);
		currentStatusPlannerPage.clickOnMapView();
		Thread.sleep(1000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
	
	}
	
	/**
	 * Test to Verify the remaining driving time of the drivers in the Asset for Current Status Screen
	 * @param method
	 * @throws InterruptedException
	 * @throws IOException
	 */

	@Test 
	public void currentStatus_004_VerifyTheRemainingDrivingTimeForTheDrivers(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the remaining driving time for the drivers available in the assets");
		Thread.sleep(8000);
	
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		CurrentStatus_PlannerPage currentStatusPlannerPage = new CurrentStatus_PlannerPage();
		Thread.sleep(2000);
		
		
		currentStatusPlannerPage.clickOnslider1();

		Thread.sleep(2000);
		currentStatusPlannerPage.clickOnslider();
		Thread.sleep(2000);
		CurrentStatus_PlannerPage.movingVehicleCapacityslider();
		
		Thread.sleep(4000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		Thread.sleep(2000);
		currentStatusPlannerPage.verifyRemainingDrivingTime();
		
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		
	}
	
	
	/**
	 * currentStatus_005_VerifyVideoCounterData(Method method) :
	 * This test is used to verify the video counter data for the driver
	 * @param method
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	
	@Test (enabled=false)
	public void currentStatus_005_VerifyVideoCounterData(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the information page visible");
		CurrentStatus_PlannerPage currentStatusPlannerPage = new CurrentStatus_PlannerPage();
		
		Thread.sleep(2000);
		
		
		currentStatusPlannerPage.clickOnslider1();

		Thread.sleep(2000);
		currentStatusPlannerPage.clickOnslider();
		Thread.sleep(2000);
		CurrentStatus_PlannerPage.movingVehicleCapacityslider();
		
		Thread.sleep(4000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		Thread.sleep(5000);
		
		int drvCount =CurrentStatus_PlannerPage.getTotalDriversInAsset();
		currentStatusPlannerPage.clickOnVideoCounterIcon(drvCount);
		Thread.sleep(2000);
		currentStatusPlannerPage.verifyVideoCounterInfoPage(drvCount);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		
	}
	
	/**
	 * Method: currentStatus_006_VerifyMapView(Method method)
	 * This method is to verify the full of the Map.
	 * @param method
	 * @throws InterruptedException
	 * @throws IOException 
	 */
	@Test  
	public void currentStatus_006_VerifyMapView(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify The Map View");
		CurrentStatus_PlannerPage currentStatusPlannerPage = new CurrentStatus_PlannerPage();
		Thread.sleep(4000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		Thread.sleep(1000);
		currentStatusPlannerPage.clickOnFullViewList();
		Thread.sleep(1000);
		currentStatusPlannerPage.clickOnMapView();
		Thread.sleep(2000);
		currentStatusPlannerPage.verifyMapView();
		Thread.sleep(1000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		
	}
	
	@Test  
	public void currentStatus_007_ClickonToggleBttn(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the user clicked on Toggle Button");
		Thread.sleep(5000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		Thread.sleep(2000);
		new CurrentStatus_PlannerPage().clickOnOnlinetoggle();
		Thread.sleep(2000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
	}

	
	@Test  
	public void currentStatus_008_clickOnVehicleBarGraph(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Click on All Bargraph Icon");
		CurrentStatus_PlannerPage currentStatusPlannerPage = new CurrentStatus_PlannerPage();
		
		Thread.sleep(7000);
		currentStatusPlannerPage.clickOnslider1();

		Thread.sleep(2000);
		currentStatusPlannerPage.clickOnslider();
		Thread.sleep(2000);
		
		CurrentStatus_PlannerPage.movingVehicleCapacityslider();
		Thread.sleep(4000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		Thread.sleep(7000);
		CurrentStatus_PlannerPage.verifyVRNExists();
		Thread.sleep(2000);
		CurrentStatus_PlannerPage.verifyExpandForVehicleGraph();
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		
	}
	
	/**
	 * This method is used to print the cance button in the application Current status screen
	 * @param method
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	@Test(enabled = false)
	public void currentStatus_009_PrintCurrentStausDriver(Method method) throws InterruptedException, AWTException
	{
		test = ExtentManager.createTest(method.getName(), "Click on print button");
		Thread.sleep(7000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBttn);
		Thread.sleep(3000);	
		CurrentStatus_PlannerPage.clickOnPrint();
		Thread.sleep(2000);
		CurrentStatus_PlannerPage.closePrintPupUp();
		
	}
	
}

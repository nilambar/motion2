package com.tis.webdriver.testcases.plannertests;

import java.io.IOException;
import java.lang.reflect.Method;

import org.sikuli.script.FindFailed;
import org.testng.annotations.Test;

import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.planner.Geofence_PlannerPage;
import com.tis.webdriver.pagefactory.pageobject.planner.PlannerPage;

public class GeoFenceTests extends NewTestTemplate
{
	
	@Test
	public void planner_GeoFence_001_VerifyPageHeader(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the page header of the GeoFence Page");
		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(5000);
		homepage.clickOnPlanner();
		PlannerPage.openGeofenceTab();
	}
	
	@Test
	public void planner_GeoFence_002_VerifyGeofenceInMap(Method method) throws InterruptedException, FindFailed, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the geofence in the Page");
		Thread.sleep(2000);
		Geofence_PlannerPage geofence = new Geofence_PlannerPage(driver);
		geofence.clickShowGeofence();
		//Thread.sleep(2000);
		//geofence.verifyGeoFenceInTheMap();
		
	}
	@Test
	public void planner_GeoFence_003_OpenSliderButtn(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the geofence in the Page");
		Thread.sleep(2000);
		PlannerPage.clickOnOpenAsset();
	}
	@Test
	public void planner_GeoFence_004_VerifyCreateGeoFencePage(Method method)
	{
		test = ExtentManager.createTest(method.getName(), "Verify the geofence in the Page");
		new Geofence_PlannerPage(driver).openCreateGeofenceIcon();
		
	}
}

package com.tis.webdriver.testcases.plannertests;

import java.io.IOException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.util.Date;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.configuration.Configuration;
import com.tis.webdriver.common.configuration.Credentials;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.planner.PlannerPage;
import com.tis.webdriver.testcases.logintests.LoginTests;
public class PlannerTests extends NewTestTemplate
{
	Credentials credentials = Configuration.getCredentials();
	
	@Test()
	public void plannerTest_001_verifyPlannerPage(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the page header of the PlannerDefault Page");
		//new LoginTests().userLoginTest();

		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(10000);
		homepage.clickOnPlanner();
		PlannerPage.verifyPageHeaderPlanner();
			
	}
	@Test()
	public void plannerTest_005_verifyTheDateRefresh(Method method) throws ParseException, InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the page header of the PlannerDefault Page");
		Date beroreRefresh = PlannerPage.retrieveTheUpdatedDate();
		Thread.sleep(60000);
		PlannerPage.clickOnRefresh();
		Date afterRefresh = PlannerPage.retrieveTheUpdatedDate();
		
		if(beroreRefresh.before(afterRefresh))
		{
			test.log(Status.INFO, "As dates are not equal the test is Pass");
			Assert.assertTrue(true);
		}
		else
		{
			Assert.assertTrue(false);
		}
	
	}
	
	@Test ()
	public void plannerTest_002_verifyAvailableSites(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the Total Sites availabe");
		PlannerPage.getAvailableSitesInDropMenu();
	}
	
	@Test ()
	public void plannerTest_003_verifyAvailableVehicles(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the Total Sites availabe");
		PlannerPage.verifyAvailableVehiclesInDropMenu();
	}
	
	@Test()
	public void plannerTest_004_verifyTheAutoRefreshCheckBox(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Test The AutoRefresh CheckBox");
		PlannerPage plannerPage =new PlannerPage(driver);
		plannerPage.clickOnAutoRefreshCheckBox();
		plannerPage.verifyAutoRefreshCheckBox();
	}
	

}

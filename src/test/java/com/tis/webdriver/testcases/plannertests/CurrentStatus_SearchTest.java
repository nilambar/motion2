package com.tis.webdriver.testcases.plannertests;

import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.dashboardpage.DashboardPage;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.planner.CurrentStatus_PlannerPage;
import com.tis.webdriver.pagefactory.pageobject.planner.PlannerPage;


public class CurrentStatus_SearchTest extends NewTestTemplate
{
	By sliderBtn = By.cssSelector("#btn-opener>img");
	
	@BeforeTest
	public void setUP() throws InterruptedException
	{
		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(10000);
		homepage.clickOnPlanner();
		PageFactory.initElements(driver, CurrentStatus_PlannerPage.class);
		
	}
	
	@Test 
	public void currentStatus_001_SerchDriverInAsset(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Search Driver");
		PlannerPage currentStatus=new PlannerPage(driver);
		Thread.sleep(6000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBtn);

		CurrentStatus_PlannerPage currentStatusPlannerPage = new CurrentStatus_PlannerPage();
		Thread.sleep(4000);
		currentStatusPlannerPage.clickOnslider1();

		Thread.sleep(2000);
		currentStatusPlannerPage.clickOnslider();
		Thread.sleep(2000);
		CurrentStatus_PlannerPage.movingVehicleCapacityslider();
		
		Thread.sleep(4000);
		PlannerPage.clickOnSearch();
		Thread.sleep(5000);
		String dName=CurrentStatus_PlannerPage.getDriverName();
		currentStatus.searchDriverVehicleOnMap(dName);
		//currentStatus.searchDriverVehicleOnMap("ARNAUD EMMANUEL");
		Thread.sleep(2000);
		CurrentStatus_PlannerPage.veriFyTheSearch(dName);
		try {
			Thread.sleep(2000);
			CurrentStatus_PlannerPage.closeSearchTxtBox();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		Thread.sleep(3000);
		//new PlannerPage(driver).openDriverVehicleAsset(sliderBtn);
		
		
	}
	@Test
	public void currentStatus_002_SearchVehicleInAsset(Method method) throws InterruptedException, IOException
	{
		test=createTest(method.getName(), "Search Vehicle in Asset");
		Thread.sleep(3000);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBtn);
		//driver.findElement(By.cssSelector("#menu-button>a>img")).click();
		
		Thread.sleep(2000);
		CurrentStatus_PlannerPage currentStatusPlannerPage = new CurrentStatus_PlannerPage();
		Thread.sleep(4000);
		currentStatusPlannerPage.clickOnslider1();

		Thread.sleep(2000);
		currentStatusPlannerPage.clickOnslider();
		Thread.sleep(2000);
		CurrentStatus_PlannerPage.movingVehicleCapacityslider();
				
		Thread.sleep(3000);
		
		CurrentStatus_PlannerPage.clickOnVehicleTab();
		
	
		PlannerPage currentStatus=new PlannerPage(driver);
		Thread.sleep(2000);
		PlannerPage.clickOnSearch();
		Thread.sleep(2000);
		
		String vName=CurrentStatus_PlannerPage.getVehicleName();
		Thread.sleep(2000);
		currentStatus.searchDriverVehicleOnMap(vName);
		//currentStatus.searchDriverVehicleOnMap("ARNAUD EMMANUEL");
		Thread.sleep(2000);
		CurrentStatus_PlannerPage.veriFyTheSearch(vName);
		new PlannerPage(driver).openDriverVehicleAsset(sliderBtn);
	}
	@Test
	public void currentStatus_003_VerifySearchSuggestions(Method method) throws InterruptedException, IOException
	{
		test = createTest(method.getName(), "Verify the search suggestions");
		//new TisHomePage(driver).clickOnPlanner();
		new PlannerPage(driver).openDriverVehicleAsset(sliderBtn);
		Thread.sleep(5000);
		PlannerPage.clickOnSearch();
		new PlannerPage(driver).searchSuggestions("test");
		new PlannerPage(driver).openDriverVehicleAsset(sliderBtn);
	}
}

package com.tis.webdriver.testcases.reportstests;

import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.reports.ReportsFavPage;
import com.tis.webdriver.pagefactory.pageobject.reports.ReportsPage;

public class ReportsFavouriteTests extends NewTestTemplate
{
	@BeforeTest
	public void clickOnReports() throws InterruptedException
	{
		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(15000);
		homepage.clickOnReports();
		PageFactory.initElements(driver, ReportsFavPage.class);
		
	}
	
	
	@Test
	public void reports_001_VerifyFavouriteReport_DriverTab(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify Report Fav in Driver Tab");
		Thread.sleep(2000);
		ReportsFavPage.checkIfFavReportsExist();
		ReportsFavPage.clickOnFavIconsInReports();
		Thread.sleep(5000);
		ReportsFavPage.verifyFavouritesAddedInFavouriteSection();
		
	}
	
	@Test
	public void reports_002_VerifyFavouriteReport_VehiclesTab(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify Report Fav in Vehicle Tab");
		Thread.sleep(3000);
		ReportsFavPage.clickOnVehicleTab();
		ReportsFavPage.checkIfFavReportsExist();
		ReportsFavPage.clickOnFavIconsInReports();
		Thread.sleep(3000);
		ReportsFavPage.verifyFavouritesAddedInFavouriteSection();
		
	}
	

	
	
}

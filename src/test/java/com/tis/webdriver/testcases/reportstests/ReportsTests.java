package com.tis.webdriver.testcases.reportstests;

import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.reports.ReportsPage;

public class ReportsTests extends NewTestTemplate
{
	@BeforeTest
	public void clickOnReports() throws InterruptedException
	{
		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(15000);
		homepage.clickOnReports();
		PageFactory.initElements(driver, ReportsPage.class);
	}
	
	@Test (enabled = false)
	public void reports_001_DriverReportSearchVerifications(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify The Search report functionality");
		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(10000);
		homepage.clickOnReports();
		 
		ReportsPage reportsPage = new ReportsPage();
		//ReportsPage.verifyReportPage();
		reportsPage.searchReports("d");
		reportsPage.VerifySearchBoxFunctionality("Driver Shift Protocol");
		
	}
	@Test 
	public void reports_002_DriverEventDetailsSearchVerification(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Searching Of EventDetails Reports");
				
	//	ReportsPage reportsPage = new ReportsPage();
		
	//	reportsPage.searchReports("Event Details");
		
	//	reportsPage.VerifyEventDetailsReportOnScreen("Event Details");
		
	}
	@Test
	public void reports_005_OpenDriverEventDetailsReport(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Opening of EventDetails Reports");
		Thread.sleep(8000);
		
		ReportsPage.clickOnDriverTab(); 
		ReportsPage.clicKSite();
		ReportsPage.selectSites();
		ReportsPage.clickSelectAll();
		new ReportsPage().searchReports("Event Details");
		new ReportsPage().verifyEventDetailsReport();
		Thread.sleep(2000);
		ReportsPage.clickOnEventReportsCheckBox();
		Thread.sleep(2000);
		ReportsPage.clickOnReportGenerateBttn();
		ReportsPage.verifyDriverEventDetailProtocolRepots();
		ReportsPage.closeReport();
		ReportsPage.cancelReportSelectionCriteria();
	
	}
	@Test
	public void reports_003_OpenDriverShiftProtolReports(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Opening of  DriverShiftProtocol Reports");
		Thread.sleep(5000);
		ReportsPage.clickOnDriverTab(); 
		ReportsPage.clickSelectAll();
		new ReportsPage().searchReports("Driver Shift Protocol");
		Thread.sleep(3000);
		ReportsPage.verifyReportPage();
		Thread.sleep(2000);
		ReportsPage.clickOnReportGenerateBttn();
		ReportsPage.verifyDriverShiftProtocolRepots();
		ReportsPage.closeReport();
		ReportsPage.cancelReportSelectionCriteria();
		
	}
	
	
	@Test
	public void reports_006_VehicleEventDetailsSearchInVehicleTab(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Opening of  Event Details Report in Vehicle Tab");
		Thread.sleep(2000);
		ReportsPage.clickOnVehicleTab();
		
		ReportsPage.clicKSite();
		ReportsPage.selectSites();
		ReportsPage.clickSelectAll_Vehicle();
		new ReportsPage().searchReports("Event Details");
		new ReportsPage().verifyEventDetailsReport();
		ReportsPage.clickOnEventReportsCheckBox();
		Thread.sleep(2000);
		ReportsPage.clickOnReportGenerateBttn();
		ReportsPage.verifyDriverEventDetailProtocolRepots();
		ReportsPage.closeReport();
		ReportsPage.cancelReportSelectionCriteria();
		
	}

	
}

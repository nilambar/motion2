package com.tis.webdriver.testcases.dashboardtests;

import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.dashboardpage.DashboardPage;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;


public class DashboardTests extends NewTestTemplate
{
	@BeforeTest
	public void setUp() throws InterruptedException
	{
		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(10000);
		homepage.clickOnDashBoard();
		PageFactory.initElements(driver, DashboardPage.class);
	}
	
	@Test
	public void test_001_verifyHeaderPage(Method method) throws IOException, InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify header in the Dashboad page");
		DashboardPage.verifyPageHeader();
	}
	@Test
	public void test_002_verifyTasksTODOPopup(Method method) throws IOException, InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify The Tasks To Do pop up");
		DashboardPage.clickOnTaskToDoPupUp();
		DashboardPage.verifyTasksToDoPopUp();
		Thread.sleep(1000);
		DashboardPage.closePopUp();
	}
	@Test
	public void test_003_verifyAvailableDriver(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify available Drivers pop up");
		DashboardPage.clickOnAvailableDriversPopUp();
		Thread.sleep(2000);
		DashboardPage.verifyAvailableDriverPopUp();
		Thread.sleep(1000);
		DashboardPage.closeAvailableDriversPopUp();
		
	}
	
	@Test
	public void test_004_VerifyGoToNotificationsButtonIsWorking(Method method) throws IOException, InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the Go to Notifications Button in Tasks To Do pupup");
		DashboardPage.clickOnTaskToDoPupUp();
		DashboardPage.clickOnGoToNotification();
		Thread.sleep(3000);
		DashboardPage.verifyNotificationsPage();
		Thread.sleep(5000);
		new TisHomePage(driver).clickOnDashBoard();
		
	}
	@Test
	public void test_005_VerifyGoToDriver(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify the Go To Driver button in Available Drivers pop up");
		Thread.sleep(3000);
		DashboardPage.clickOnAvailableDriversPopUp();
		Thread.sleep(3000);
		DashboardPage.clickOnGoToDrivers();
		Thread.sleep(2000);
		DashboardPage.verifyDriversPage();
		new TisHomePage(driver).clickOnDashBoard();
	}

}

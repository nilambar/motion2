package com.tis.webdriver.testcases.notificationstests;

import java.io.IOException;
import java.lang.reflect.Method;
import java.text.ParseException;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.notifications.NotificationsListPage;
import com.tis.webdriver.pagefactory.pageobject.notifications.NotificationsPage;
import com.tis.webdriver.pagefactory.pageobject.notifications.NotificationsSeverityPage;

public class ActiveNotificationsListTests extends NewTestTemplate
{
	@BeforeTest
	public void setUP() throws IOException, InterruptedException
	{
		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(10000);
		homepage.clickNotification();
	//	NotificationsPage.clickOnFirstDropDown();
		Thread.sleep(3000);
	//	NotificationsPage.clickOnFirstDropDown();
		Thread.sleep(3000);

		PageFactory.initElements(driver, NotificationsPage.class);
	}
	
	// verifyIsNotificationsInLastThreeMonths() - Login to be updated
	
	@Test (enabled = false)
	public void notification_001_VerifyNotificationsForLastThreeMonths(Method method) throws InterruptedException, ParseException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Verify Notifications for Last Three Months");
		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(5000);
		homepage.clickNotification();
	//	NotificationsPage.clickOnFirstDropDown();
		Thread.sleep(3000);
		NotificationsPage.clicOnTimeFilter();
		NotificationsListPage.isNotificationTimeVisible("Last Three Months");
		NotificationsListPage.getTheNotificationDates();
		Thread.sleep(2000);
		NotificationsListPage.verifyIsNotificationsInLastThreeMonths();
		
	}
	// verifyIsNotificationsInLastWeek() - Login to be updated
	
	@Test  (enabled = false)
	public void notification_002_VerifyNotificationsForLastWeek(Method method) throws ParseException, IOException, InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify Notifications for Last Week");
		NotificationsPage.clicOnTimeFilter();
		NotificationsListPage.isNotificationTimeVisible("Last Week");
		NotificationsListPage.getTheNotificationDates();
		Thread.sleep(2000);
		NotificationsListPage.verifyIsNotificationsInLastWeek();

	}
	
	// verifyIsNotificationsInLastMonth() - Login to be updated
	@Test  (enabled = false)
	public void notification_003_VerifyNotificationsForLastMonth(Method method) throws ParseException, IOException, InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify Notifications for Last Month");
		NotificationsPage.clicOnTimeFilter();
		NotificationsListPage.isNotificationTimeVisible("Last Month");
		NotificationsListPage.getTheNotificationDates();
		Thread.sleep(2000);
		NotificationsListPage.verifyIsNotificationsInLastMonth();
	}
	
	// verifyIsNotificationsInCurrentMonth() - Login to be updated
	@Test  (enabled = false)
	public void notification_004_VerifyNotificationsForCurrentMonth(Method method) throws ParseException, IOException, InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify Notifications for Current Month");
		NotificationsPage.clicOnTimeFilter();
		NotificationsListPage.isNotificationTimeVisible("Current Month");
		NotificationsListPage.getTheNotificationDates();
		Thread.sleep(2000);
		NotificationsListPage.verifyIsNotificationsInCurrentMonth();
		
	}
	
	// verifyIsNotificationsInCurrentWeek() - Login to be updated
	@Test  (enabled = false)
	public void notification_005_VerifyNotificationsForCurrentWeek(Method method) throws ParseException, IOException, InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify Notifications for Current Week");
		NotificationsPage.clicOnTimeFilter();
		NotificationsListPage.isNotificationTimeVisible("Current Week");
		NotificationsListPage.getTheNotificationDates();
		Thread.sleep(2000);
		NotificationsListPage.verifyIsNotificationsInCurrentWeek();
	}
	@Test
	public void notification_006_VeriFyCriticalNotificationsForLastThreeMonths(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Critical Notifications");

		NotificationsPage.clicOnTimeFilter();
		//new TisHomePage(driver).clickNotification();
		NotificationsPage.clicOnTimeFilter();
		NotificationsListPage.isNotificationTimeVisible("Last Three Months");
		NotificationsPage.clickOnSeverityNotifiatons("Critical");
		
		NotificationsSeverityPage.verifiySeverity("Critical");
	}	
		
	@Test
	public  void notification_007_VeriFyHighNotificationsForLastThreeMonths(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the High Notifications");

		NotificationsPage.clickOnSeverityNotifiatons("High");
		
		NotificationsSeverityPage.verifiySeverity("High");
		
	}
	
	@Test
	public  void notification_008_VeriFyMediumNotificationsForLastThreeMonths(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Medium Notifications");

		NotificationsPage.clickOnSeverityNotifiatons("Medium");
		
		NotificationsSeverityPage.verifiySeverity("Medium");
		
	}

	@Test
	public  void notification_009_VeriFyLowNotificationsForLastThreeMonths(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Low Notifications");

		NotificationsPage.clickOnSeverityNotifiatons("Low");
		
		NotificationsSeverityPage.verifiySeverity("Low");
	
	}
	
	@Test
	public void notification_010_VeriFyCriticalNotificationsForLastMonth(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Critical Notifications");
		NotificationsPage.clicOnTimeFilter();
		//new TisHomePage(driver).clickNotification();
		NotificationsPage.clicOnTimeFilter();
		NotificationsListPage.isNotificationTimeVisible("Last Month");
		NotificationsPage.clickOnSeverityNotifiatons("Critical");
		
		NotificationsSeverityPage.verifiySeverity("Critical");
	}	
		
	@Test
	public  void notification_011_VeriFyHighNotificationsForLastMonth(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the High Notifications");

		NotificationsPage.clickOnSeverityNotifiatons("High");
		
		NotificationsSeverityPage.verifiySeverity("High");
	}
	
	@Test
	public  void notification_012_VeriFyMediumNotificationsForLastMonth(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Medium Notifications");

		NotificationsPage.clickOnSeverityNotifiatons("Medium");
		
		NotificationsSeverityPage.verifiySeverity("Medium");
		
	}

	@Test
	public  void notification_013_VeriFyLowNotificationsForLastMonth(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Low Notifications");

		NotificationsPage.clickOnSeverityNotifiatons("Low");
		
		NotificationsSeverityPage.verifiySeverity("Low");

	}
	@Test
	public void notification_014_VeriFyCriticalNotificationsForCurrenttMonth(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Critical Notifications");

		NotificationsPage.clicOnTimeFilter();
		//new TisHomePage(driver).clickNotification();
		NotificationsPage.clicOnTimeFilter();
		NotificationsListPage.isNotificationTimeVisible("Current Month");
		NotificationsPage.clickOnSeverityNotifiatons("Critical");
		NotificationsSeverityPage.verifiySeverity("Critical");
	}	
		
	@Test
	public  void notification_015_VeriFyHighNotificationsForCurrenttMonth(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the High Notifications");

		NotificationsPage.clickOnSeverityNotifiatons("High");
		
		NotificationsSeverityPage.verifiySeverity("High");
		
	}
	
	@Test
	public  void notification_016_VeriFyMediumNotificationsForCurrenttMonth(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Medium Notifications");

		NotificationsPage.clickOnSeverityNotifiatons("Medium");
		
		NotificationsSeverityPage.verifiySeverity("Medium");
		
	}

	@Test
	public  void notification_017_VeriFyLowNotificationsForCurrenttMonth(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Low Notifications");

		NotificationsPage.clickOnSeverityNotifiatons("Low");
		
		NotificationsSeverityPage.verifiySeverity("Low");
		
		
	}
	@Test
	public void notification_018_VeriFyCriticalNotificationsForLastWeek(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Critical Notifications");

		NotificationsPage.clicOnTimeFilter();
		//new TisHomePage(driver).clickNotification();
		NotificationsPage.clicOnTimeFilter();
		NotificationsListPage.isNotificationTimeVisible("Last Week");
		NotificationsPage.clickOnSeverityNotifiatons("Critical");
		
		NotificationsSeverityPage.verifiySeverity("Critical");
		

	}	
		
	@Test
	public  void notification_019_VeriFyHighNotificationsForLastWeek(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the High Notifications");

		NotificationsPage.clickOnSeverityNotifiatons("High");
		
		NotificationsSeverityPage.verifiySeverity("High");
		
	}
	
	@Test
	public  void notification_020_VeriFyMediumNotificationsForLastWeek(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Medium Notifications");

		NotificationsPage.clickOnSeverityNotifiatons("Medium");
		
		NotificationsSeverityPage.verifiySeverity("Medium");
		
	}

	@Test
	public  void notification_021_VeriFyLowNotificationsForLastWeek(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Low Notifications");

		NotificationsPage.clickOnSeverityNotifiatons("Low");
		
		NotificationsSeverityPage.verifiySeverity("Low");
		
		
	}
	
	
	



}

package com.tis.webdriver.testcases.notificationstests;

import java.io.IOException;
import java.lang.reflect.Method;

import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.notifications.NotificationsPage;

public class NotificationsVerifyMoreTest extends NewTestTemplate
{
	
	
	
	@BeforeTest
	public void notificationInitiations() throws InterruptedException
	{
		PageFactory.initElements(driver, NotificationsPage.class);
		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(5000);

		homepage.clickNotification();
	}
	@Test (enabled = false)
	public void notifications_001_ClickSetAsDone(Method method)  throws InterruptedException
	{
		test =ExtentManager.createTest(method.getName(), "Verify the More Option");
	
		
		//Notifications.notificationsMore();
		Thread.sleep(8000);
		int beforeSetAsDone = NotificationsPage.totalActiveNotifications();
		
		new NotificationsPage(driver).notificationsActiveSetAsDone();
		
		new NotificationsPage(driver).clickOnConfirmBttn();
		Thread.sleep(6000);
		int afterSetAsDone = NotificationsPage.totalActiveNotifications();
		
		if (beforeSetAsDone > afterSetAsDone){
			Assert.assertTrue(true);
		}
		else
		{
			Assert.assertTrue(false);
		}

	}
	
	@Test 
	public void notifications_002_EditTest(Method method)
	{
		test =ExtentManager.createTest(method.getName(), "Edit the Notification");
		
		new NotificationsPage(driver).notificationsEdit("Edit");
	}
	
	@Test 
	public void notifications_003_VerifyMarkAsUnread(Method method) throws InterruptedException
	{
		test =ExtentManager.createTest(method.getName(), "Verify the More Option");
		Thread.sleep(2000);
	
		new NotificationsPage(driver).notificationsMarkAsUnread("Mark As Unread");
	}
	
	
	
}

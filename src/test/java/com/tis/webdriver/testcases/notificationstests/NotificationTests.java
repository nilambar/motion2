package com.tis.webdriver.testcases.notificationstests;

import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.notifications.NotificationsPage;

public class NotificationTests extends NewTestTemplate{
	

	@BeforeTest
	public void setUP() throws IOException, InterruptedException
	{
		
		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(8000);
		homepage.clickNotification();
		PageFactory.initElements(driver, NotificationsPage.class);
	}
	
	
	
	
	
	@Test
	public void notification_001_VerifyPageHeader(Method method) throws InterruptedException
	{

		test = ExtentManager.createTest(method.getName(), "Verify the page header of the Notification Page");
	
		Thread.sleep(3000);
		NotificationsPage.verifyPageHeaderNotification();
		
	}
	
	
	@Test()
	public void notification_002_ActiveTabsVerification(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify The Active Tabs Fucntionality");
		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(5000);
	//	homepage.clickNotification();
		NotificationsPage.verifyThenumberOfavailableNotifications();
		NotificationsPage.totalActiveNotifications();
		NotificationsPage.totalCompletedNotifications();
	}
	
		
	
	@Test
	public void notification_003_verify(Method method)
	{
		NotificationsPage.sevListItems();
	}
	
	@Test(enabled = false)
	public void notification_003_VerifyNotificationsSeverity(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify Notificaton Page displayed");
		//TisHomePage homepage = new TisHomePage(driver);
		//homepage.clickOnPlanner();
		//homepage.clickNotification();
		//Notifications.clickAllNotificationsFromActive();
		//Notifications.criticalNotifcationsInActiveTab("High");
		//Notifications.criticalNotificationsFromPage();
		NotificationsPage.highNotifications();
		NotificationsPage.lowNotifications();
		NotificationsPage.mediumNotifications();
		Thread.sleep(3000);
		System.out.println("The number of total nnnnnnn: "+NotificationsPage.totalCompletedNotifications());
		NotificationsPage notifications = new NotificationsPage(driver);
		notifications.unReadNotifications();
		
	}
	@Test(enabled = false)
	public void notification_004_VerifyTotalActiveNotifications(Method method)
	{
		test = ExtentManager.createTest(method.getName(), "Verify  total notifications");
		NotificationsPage.verifyActiveNotifications();
	}
	@Test(enabled = false)
	public void notification_005_VerifyTotalCompletedNotifications(Method method)
	{
		test = ExtentManager.createTest(method.getName(), "Verify total completed notifications");
		NotificationsPage.verifyCompletedNotifications();
	}

	
	@Test(enabled = false)
	public void notification_006_FilterNotificationsByTime(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify time filter notifications");
	
		NotificationsPage.getValueFromTimeFilter();
		
	}
	@Test(enabled = false)
	public void notification_007_FilterNotificationBySorting(Method method) throws InterruptedException
	{
		test = ExtentManager.createTest(method.getName(), "Verify sort filter notifications");
		NotificationsPage.getValueFromSortingFilter();
		
	}
	@Test(enabled = false)
	public void notification_006_FilterNotificationBySite()
	{
		
	}
	@Test(enabled = false)
	public void notification_007_FilterNotificationByNotificationTypeNSeverity()
	{
		
	}
	@Test (enabled = false)
	public void notification_008_ReOpenNotification(Method method)
	{
		test=ExtentManager.createTest(method.getName(), "ReOpen a completed notification");
		new NotificationsPage(driver).openCompletedNotification();
		new NotificationsPage(driver).clikcOnBttnReOpen();
		
	}
	@Test (enabled = false)
	public void notification_009_VerifyNotificationInCompletedTab(Method method) throws InterruptedException
	{
		test=ExtentManager.createTest(method.getName(), "Verify Notification Moved to Completed Tab");
		new NotificationsPage(driver).getVehicleInCompletedOrActiveTab();
		
		new NotificationsPage(driver).verifyNotification();
		
	}
	@Test (enabled = false)
	public void notifications_010_GetNotificationsInActiveTab(Method method) throws InterruptedException
	{
		test=ExtentManager.createTest(method.getName(), "GetTheNotification");
		NotificationsPage.clickAllNotificationsFromActive();
		NotificationsPage.getAllNotificationsInActiveTab();
	}

}

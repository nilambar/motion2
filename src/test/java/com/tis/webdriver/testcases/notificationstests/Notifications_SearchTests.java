package com.tis.webdriver.testcases.notificationstests;

import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.notifications.NotificationsListPage;
import com.tis.webdriver.pagefactory.pageobject.notifications.NotificationsPage;
import com.tis.webdriver.pagefactory.pageobject.planner.CurrentStatus_PlannerPage;
import com.tis.webdriver.pagefactory.pageobject.planner.PlannerPage;
import com.tis.webdriver.testcases.logintests.LoginTests;

public class Notifications_SearchTests extends NewTestTemplate
{
	@BeforeTest
	public void setUp() throws InterruptedException
	{
		//Thread.sleep(5000);
		
		PageFactory.initElements(driver, NotificationsPage.class);
		
		
	}
	@Test
	public void notificationsSearch_001_SerchInActiveTab(Method method) throws InterruptedException, IOException
	{
		test = ExtentManager.createTest(method.getName(), "Search Driver");
		
		new TisHomePage(driver).clickNotification();
		Thread.sleep(5000);
		String name = NotificationsPage.getDnameOrVRNFromList();
		//Thread.sleep(5000);
		new NotificationsPage(driver).clickOnSearchIcon();
	
		new NotificationsPage(driver).searchDriverOrVehicle(name);
		Thread.sleep(2000);
		new NotificationsPage(driver).verifySearchInList(name);
		//new PlannerPage(driver).closeSearchTextBox();

	}
	@Test 
	public void notificationsSearch_003_SearchInCompletedTab(Method method) throws InterruptedException, IOException
	{
		test=createTest(method.getName(), "Search Verification for Completed Tab");
		new NotificationsPage(driver).openCompletedNotification();
		
		
		
		String name = NotificationsPage.getDnameOrVRNFromList();
		Thread.sleep(5000);
		new NotificationsPage(driver).clickOnSearchIcon();
	
		new NotificationsPage(driver).searchDriverOrVehicle(name);
		Thread.sleep(2000);
		new NotificationsPage(driver).verifySearchInList(name);
	}
	@Test 
	public void notificationsSearch_002_VerifySearchSuggestionsInActiveTab(Method method) throws InterruptedException, IOException
	{
		test = createTest(method.getName(), "Verify the search suggestions");
		new TisHomePage(driver).clickNotification();
		Thread.sleep(10000);
		new NotificationsPage(driver).clickOnSearchIcon();
		new PlannerPage(driver).searchSuggestions("Peter");
		new PlannerPage(driver).closeSearchTextBox();
		
	}
	
	@Test 
	public void notificationsSearch_004_VerifySearchSuggestionsInCompletedTab(Method method) throws InterruptedException, IOException
	{
		test = createTest(method.getName(), "Verify the search suggestions");
		new TisHomePage(driver).clickNotification();
		Thread.sleep(10000);
		new NotificationsPage(driver).clickOnSearchIcon();
		new PlannerPage(driver).searchSuggestions("Peter");
		new PlannerPage(driver).closeSearchTextBox();
		
	}

}

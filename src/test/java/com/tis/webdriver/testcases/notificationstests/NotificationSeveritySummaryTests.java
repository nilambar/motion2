package com.tis.webdriver.testcases.notificationstests;

import java.io.IOException;
import java.lang.reflect.Method;






import java.util.ArrayList;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;
import com.tis.webdriver.pagefactory.pageobject.notifications.NotificationsListPage;
import com.tis.webdriver.pagefactory.pageobject.notifications.NotificationsPage;
import com.tis.webdriver.pagefactory.pageobject.notifications.NotificationsSeverityPage;


public class NotificationSeveritySummaryTests extends NewTestTemplate
{
	@BeforeTest
	public void setUP() throws IOException, InterruptedException
	{
		
		TisHomePage homepage = new TisHomePage(driver);
		Thread.sleep(8000);
		homepage.clickNotification();
		PageFactory.initElements(driver, NotificationsPage.class);
	}
	
	
	@Test
	public void notifications_001_VeriFyCriticalNotificationsForLastThreeMonths(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Critical Notifications");

	
		new TisHomePage(driver).clickNotification();
		NotificationsPage.clicOnTimeFilter();
		NotificationsListPage.isNotificationTimeVisible("Last Three Months");
		NotificationsPage.clickOnSeverityNotifiatons("Critical");
		
		NotificationsSeverityPage.verifiySeverity("Critical");
		

	}	
		
	@Test
	public  void notifications_002_VeriFyHighNotifications(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the High Notifications");
		//new TisHomePage(driver).clickNotification();
		//NotificationsPage.clicOnTimeFilter();
		//NotificationsListPage.isNotificationTimeVisible("Last Three Months");
		NotificationsPage.clickOnSeverityNotifiatons("High");
		
		NotificationsSeverityPage.verifiySeverity("High");
		
	}
	
	@Test
	public  void notifications_003_VeriFyMediumNotifications(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Medium Notifications");
		//new TisHomePage(driver).clickNotification();
		//NotificationsPage.clicOnTimeFilter();
		//NotificationsListPage.isNotificationTimeVisible("Last Three Months");
		NotificationsPage.clickOnSeverityNotifiatons("Medium");
		
		NotificationsSeverityPage.verifiySeverity("Medium");
		
	}

	@Test
	public  void notifications_004_VeriFyLowNotifications(Method method) throws InterruptedException, IOException
	{
		test=ExtentManager.createTest(method.getName(), "Verify the Low Notifications");
		//new TisHomePage(driver).clickNotification();
		//NotificationsPage.clicOnTimeFilter();
		//NotificationsListPage.isNotificationTimeVisible("Last Three Months");
		NotificationsPage.clickOnSeverityNotifiatons("Low");
		
		NotificationsSeverityPage.verifiySeverity("Low");
		
		
	}
}

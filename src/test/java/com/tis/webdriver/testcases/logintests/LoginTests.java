package com.tis.webdriver.testcases.logintests;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.configuration.Configuration;
import com.tis.webdriver.common.configuration.Credentials;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;
import com.tis.webdriver.pagefactory.pageobject.loginarea.LoginArea;

public class LoginTests extends NewTestTemplate

{
	
	Screen screen = new Screen();
	Pattern switchtoTis = new Pattern("C:\\WorkSapace\\ScreenShorts\\SwitchToTIS.PNG");
	Pattern reCenterClick = new Pattern("C:\\WorkSapace\\ScreenShorts\\ReCenterIconClick.PNG");
	Pattern clickVehicle = new Pattern("C:\\WorkSapace\\ScreenShorts\\Vehicleimg.PNG");
	
	
	Credentials credentials = Configuration.getCredentials();
	@Test
	public void userLoginTest() throws IOException, InterruptedException
	{
		test = ExtentManager.createTest("userLoginTest", "Verify User Login");
		TisBasePageObject base = new TisBasePageObject();
		//base.openTisPage(tisURL);
		LoginArea login = new LoginArea(driver);
		
		login.userLogin(credentials.accountName,credentials.useName, credentials.passWord);
		
		login.clickLoginbttnToLogin();
		base.verifyUserLoggedIn();
		Thread.sleep(4000);
		TisBasePageObject.clickOnTisMotion2();
		Thread.sleep(3000);
		TisBasePageObject.clickOnTisMotion2Link();
		
	}
	
	
	@Test (enabled = false)
	public void navigateToTis()
	{
		test = ExtentManager.createTest("Motion 2 ", "Verify User Login");
		
	}
	
	@Test (enabled = false)
	public void loadMapPage() throws InterruptedException, FindFailed
	{
		WebDriverWait wait = new WebDriverWait(driver, 10);
		
		test=ExtentManager.createTest("LoadMapPage", "Verify User Login");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='myTab']/li[4]/a")));
		
		
		driver.findElement(By.xpath(".//*[@id='myTab']/li[4]/a")).click();
		Thread.sleep(10000);
		
		screen.click(switchtoTis);
		test.log(Status.INFO, "User clicked on Switch to TIS button");
		Thread.sleep(30000);
		
		driver.findElement(By.xpath(".//*[@id='vehiclechildDiv']/div[1]/div[1]/div[1]/div[3]/i[2]")).click();
		
		
		
		
		//screen.click(reCenterClick);
		Thread.sleep(5000);
	
			screen.click(clickVehicle);
		
		
		  String windowhandle =driver.getWindowHandle();
		  System.out.println(windowhandle);
		  driver.switchTo().window(windowhandle);
		  Thread.sleep(1000);
		 
		  driver.findElement(By.xpath("html/body/div[3]/div[2]/div[1]/div/div/div[3]/button[1]")).click();
		
		 
		
		Thread.sleep(8000);		
	}
	@Test (enabled = false)
	public void logoutTest() throws InterruptedException, IOException
	{
		test=ExtentManager.createTest("logoutTest", "Verify user logged out");
		TisBasePageObject base = new TisBasePageObject();
		Thread.sleep(1000);
		
			base.logOut();
		//	base.verifyUserLoggedOut();
		
	
	}
	
	
	/*@AfterMethod(enabled = false)
	public void getResult(ITestResult result)
	{
		if(result.getStatus() == ITestResult.FAILURE){
			test.log(Status.FAIL, "Test Case Failed is "+result.getName());
			test.log(Status.FAIL, "Test Case Failed is "+result.getThrowable());
		}else if(result.getStatus() == ITestResult.SKIP){
			test.log(Status.SKIP, "Test Case Skipped is "+result.getName());
		}
		// ending test
		//endTest(logger) : It ends the current test and prepares to create HTML report
		
	}
	@AfterSuite
	public void quit()
	{
	driver.close();
	driver.quit();
	}
*/
	

}

package com.tis.webdriver.pagefactory.pageobject;

import java.io.IOException;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.core.element.Wait;
import com.tis.webdriver.common.template.ExtentManager;


public class TisBasePageObject extends BasePageObject
{
	@FindBy (css = ".pageheader>h2")
	WebElement switchboard;
	
	@FindBy (xpath = ".//*[@id='navbar']/div[2]/ul/li[3]/a/i")
	WebElement logOutdrop;
	
	@FindBy (xpath = ".//*[@id='navbar']/div[2]/ul/li[3]/ul/li[4]/a")
	WebElement logOutLink;
	
	@FindBy (xpath = ".//*[@id='btn_yes']")
	WebElement clickYes;
	
	@FindBy (xpath = ".//*[@id='btnLogin']")
	WebElement loginbttn;
	@FindBy(xpath = "//a[text()='TIS-Web Motion']")
	static 	WebElement tisMotion2;
	@FindBy(xpath = ".//*[@id='btnSwitchToMapping']")
	static WebElement clickOnMothion2;
		
	public TisBasePageObject()
	{
		super();
	}
	


	public void openTisPage(String tisURL) 
	{
		getURL(tisURL);
			
	}
	public void verifyUserLoggedIn() throws IOException, InterruptedException 
	{

		
		if (switchboard.isDisplayed())
		{
			Assert.assertTrue(true);
			Thread.sleep(1000);
			test.log(Status.PASS, "Home Page displayed       : " + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "loginTest")));
		}
		else{
			Assert.assertTrue(false);
			
		}
	}

	public boolean switchToWindowByTitle(String title){
	    String currentWindow = driver.getWindowHandle(); 
	    Set<String> availableWindows = driver.getWindowHandles(); 
	    if (!availableWindows.isEmpty()) { 
	         for (String windowId : availableWindows) {
	              String switchedWindowTitle=driver.switchTo().window(windowId).getTitle();
	              if ((switchedWindowTitle.equals(title))||(switchedWindowTitle.contains(title))){ 
	                  return true; 
	              } else { 
	                driver.switchTo().window(currentWindow); 
	              } 
	          } 
	     } 
	     return false;
	}

	public void logOut() throws InterruptedException 
	{
	//	WebElement we = wait.forElementVisible(logOutdrop);
		if (logOutdrop.isDisplayed())
		{
			logOutdrop.click();
			Thread.sleep(1000);
			String windowhandle =driver.getWindowHandle();
			System.out.println(windowhandle);
			driver.switchTo().window(windowhandle);
			Thread.sleep(1000);
  		  	driver.findElement(By.xpath(".//*[@id='btn_yes']")).click();
		}
		else
		{
			System.out.println("No logout link provided");
			Assert.assertTrue(false);
		}
	
	}



	public void verifyUserLoggedOut() throws IOException
	{
		//wait.forElementVisible(loginbttn);
		if (loginbttn.isDisplayed())
		{
		 	Assert.assertTrue(true, "logout successful");
			test.log(Status.PASS, "User Logged out from the system");
		}
		else
		{
			Assert.assertTrue(false, "logout successful");
			test.log(Status.FAIL, "Error: " + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "loginTest")));
		}
		
		
	}



	public static void clickOnTisMotion2() throws InterruptedException
	{
		Thread.sleep(2000);
		wait.forElementClickable(tisMotion2);
		
		tisMotion2.click();
		
	}
	public static void clickOnTisMotion2Link() throws InterruptedException
	{
		Thread.sleep(2000);
		wait.forElementClickable(clickOnMothion2);
		clickOnMothion2.click();
		test.log(Status.INFO, "Motion 2 button clicked");
	}

}

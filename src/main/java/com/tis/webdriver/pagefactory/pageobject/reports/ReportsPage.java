package com.tis.webdriver.pagefactory.pageobject.reports;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.html5.AddApplicationCache;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.model.Log;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;

public class ReportsPage extends TisBasePageObject
{
	@FindBy (css = "input[type='search']")
	static WebElement searchTextBox;
	@FindBy (xpath = ".//*[@id='my-reports']/div[2]/div/div[1]/div[1]/span")
	static WebElement eventDetailsReports;
	@FindBy (xpath=".//*[@id='drivers-list']/div/div/main[1]")
	static WebElement driversGrid;
	@FindBy (xpath = ".//*[@id='drivers-list']/div/div/main[2]")
	static WebElement vehiclesGrid;
	@FindBy (xpath = ".//*[@id='drivers-list']/div/div/main[2]/ul")
	static WebElement availableVehicles;
	@FindBy (xpath=".//*[@id='drivers-list']/div/div/main[1]/ul")
	static WebElement availableDrivers;
	@FindBy (xpath = "//div[@class='checkbox-def checkbox-inline checkboxmaster checkbox-primary']")
	static WebElement chkBoxSelectAllDrivers;
	@FindBy (xpath = "//span[contains (text(),'Driver Shift Protocol')]")
	static WebElement rptDriverShiftProtocol;
	@FindBy (xpath = "//span[contains(text(),'report selection criteria')]")
	static WebElement reportSelectionCriteriaPage;
	@FindBy (xpath = "//button[@class='btn btn-secondary blue-button-2 bottom-navbar-generate']")
	static WebElement bttnGenerate;
	@FindBy (xpath = ".//*[@id='dropdownMenuButton1']")
	static WebElement sitesDropDown;
	@FindBy (xpath = "//div[@class='dropdown-menu top-blue-list']/a")
	static WebElement availableSites;
	@FindBy (css = "div[class='card-content']")
	static WebElement reports;
	@FindBy (xpath  ="//h2[contains(text(),'Driver Shift Protocol')]")
	static List<WebElement> driverShiftProtocol;
	@FindBy (xpath  ="//h2[contains(text(),'Event Details')]")
	static List<WebElement> driverEventDetailsProtocol;
	@FindBy (xpath  ="//h2[contains(text(),'Event Details')]")
	static List<WebElement> eventDetails;
	@FindBy (xpath = "//i[@title='close' and @class='glyphs blue-dark btn-close-report-preview']")
	static WebElement closeReport;
	@FindBy (xpath = "//button[@class='btn btn-secondary blue-button bottom-navbar-cancel']")
	static WebElement cancelReportSelectionCriteria;
	
	@FindBy (xpath = "//div[@class='checkbox-def checkbox-inline checkboxmaster checkbox-primary']/input")
	static WebElement chkbox;
	@FindBy (xpath=".//*[@id='btn-drivers-legal']/span")
	static WebElement vehiclesTabInReport;
	@FindBy (xpath=".//*[@id='btn-drivers-overview']/span")
	static WebElement driversTab;
	
	//@FindBy (xpath = "//ul[@class='reports-table-reports-content']/li/input")
	
	
	public ReportsPage searchReports(String reportTyep) throws InterruptedException
	{
		wait.forElementVisible(searchTextBox, 30);
		Thread.sleep(5000);
		searchTextBox.clear();
		searchTextBox.sendKeys(reportTyep);
		Thread.sleep(5000);
		
		List<WebElement> rptInAvailablLists = driver.findElements(By.xpath("//span[@class='completer-list-item']"));
		
		if(rptInAvailablLists.size()>0)
		{
			for(WebElement ele:rptInAvailablLists)
			{
				if(ele.getText().contains(reportTyep))
				{
					ele.click();
				}
			}
		}
		else
		{
			Assert.assertTrue(false);
			
		}
	
		return this;
		
	}
	public ReportsPage VerifySearchBoxFunctionality(String repots) throws IOException
	{
		
		List<WebElement>reportsList1=driver.findElements(By.cssSelector(".completer-dropdown"));
		for(WebElement ele:reportsList1)
		{
			if(ele.getText().contains(repots))
			{
				test.log(Status.PASS, "Report Search is working Fine" +test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "reports")));
			}
			else
			{
				test.log(Status.FAIL, "Reports Search is not working" +test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "failedreports")));
				Assert.assertTrue(false);
				
			}
		}
		return this;
	}
	public void VerifyEventDetailsReportOnScreen(String repots) throws IOException 
	{
		VerifySearchBoxFunctionality(repots);
		wait.forElementVisible(eventDetailsReports);
		Assert.assertTrue(true);
		test.log(Status.PASS, "Reports is available" +test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "searched reports")));
		
	}
	public void verifyEventDetailsReport() throws InterruptedException
	{
		Thread.sleep(5000);
		if(driver.findElement(By.xpath("//span[contains(text(),'Event Details')]")).isDisplayed())
		{
			List<WebElement> divs = driver.findElements(By.cssSelector("div[class='card-content']"));
			
			for(WebElement ele:divs)
			{
				System.out.println(ele.getSize());
			}
			driver.findElement(By.xpath("//span[contains(text(),'Event Details')]")).click();
			test.log(Status.INFO, "Reports clicked");
		}
		else
		{
		  Assert.assertTrue(false);
		}
		
	}
	public static void clickSelectAll() throws InterruptedException
	{
		Thread.sleep(5000);
		int driversCount =ReportsPage.getAvailableDriversInReportPage();
		if (driversCount > 0)
		{

			wait.forElementClickable(chkBoxSelectAllDrivers);
			
			if (chkbox.isSelected())
			{
				System.out.println("Checkbox Selected");
			}
			else
			{
				chkBoxSelectAllDrivers.click();
			}
		}
		
	}
	
	public static void clickSelectAll_Vehicle() throws InterruptedException
	{
		Thread.sleep(5000);
		int driversCount =ReportsPage.getAvailableVehiclesInReportPage();
		if (driversCount > 0)
		{

			wait.forElementClickable(chkBoxSelectAllDrivers);
			
			if (chkbox.isSelected())
			{
				System.out.println("Checkbox Selected");
			}
			else
			{
				chkBoxSelectAllDrivers.click();
			}
		}
		
	}
	public static int getAvailableDriversInReportPage()
	{
		wait.forElementVisible(driversGrid);
		ArrayList<String> drivers = new ArrayList<String>();
		List<WebElement> driversList = driver.findElements(By.xpath(".//*[@id='drivers-list']/div/div/main[1]/ul"));
		for(WebElement ele:driversList)
		{
			drivers.add(ele.getText());
			
		}
		System.out.println(drivers.size());
		return drivers.size();
	}
	
	public static int getAvailableVehiclesInReportPage()
	{
		wait.forElementVisible(vehiclesGrid);
		ArrayList<String> drivers = new ArrayList<String>();
		List<WebElement> driversList = driver.findElements(By.xpath(".//*[@id='drivers-list']/div/div/main[2]/ul"));
		for(WebElement ele:driversList)
		{
			drivers.add(ele.getText());
			
		}
		System.out.println(drivers.size());
		return drivers.size();
	}
	public static void  verifyReportPage() throws InterruptedException
	{
		
		if(reports.isDisplayed())
		{
			List<WebElement> divs = driver.findElements(By.cssSelector("div[class='card-content']"));
			
			for(WebElement ele:divs)
			{
				System.out.println(ele.getSize());
			}
			Thread.sleep(2000);
			driver.findElement(By.xpath("//span[contains(text(),'Driver Shift Protocol')]")).click();
			test.log(Status.INFO, "Reports clicked");
		}
		else
		{
		  Assert.assertTrue(false);
		}
		
		
	//	wait.forElementClickable(rptDriverShiftProtocol, 10);
		//rptDriverShiftProtocol.click();
	}
	
	public static void reportSelectionCriteriaPage()
	{
		wait.forElementVisible(reportSelectionCriteriaPage, 5);
		
	}
	public static void clickOnReportGenerateBttn() throws InterruptedException
	{
		Thread.sleep(2000);
		wait.forElementClickable(bttnGenerate);
		bttnGenerate.click();
	}
	public static void clicKSite() throws InterruptedException
	{
		Thread.sleep(5000);
		wait.forElementClickable(sitesDropDown);
		sitesDropDown.click();
	}
	public static void selectSites()
	{
		
		wait.forElementVisible(availableSites);
		List<WebElement> sitesList = new ArrayList<WebElement>();
		List<WebElement> sites = driver.findElements(By.xpath("//div[@class='dropdown-menu top-blue-list']/a"));
		for(WebElement ele:sites )
		{
			System.out.println(ele.getText());
			if (ele.getText().contentEquals("BASE"))
			{
				ele.click();
				break;
			}
			
		}
		
		
	}
	public static void verifyDriverShiftProtocolRepots() throws IOException 
	{
		if(driverShiftProtocol.isEmpty())
		{
			Assert.assertTrue(false);
		}
		else
		{		test.log(Status.PASS, "Driver Shift Protocol Visible" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "searched reports")));
		
		}
		
	}
	public static void closeReport() throws InterruptedException 
	{
		
		Thread.sleep(5000);
		wait.forElementClickable(closeReport);
		Thread.sleep(2000);
		closeReport.click();
		test.log(Status.INFO, "Reports Closed");
		
	}
	public static void clickOnEventReportsCheckBox() throws InterruptedException
	{
		List<WebElement> chkboxes = driver.findElements(By.xpath("//ul[@class='reports-table-reports-content']/li/input"));
		if (chkboxes.size() > 0)
			{
				for (WebElement ele:chkboxes)
				{
					if(ele.isSelected())
					{
						System.out.println("CheckBox already selected ");
					}
					else
					{
						Thread.sleep(500);
						ele.click();
					}
					
					
					
			
				}
			}
	}
	
	public static void cancelReportSelectionCriteria() throws InterruptedException
	{
		Thread.sleep(2000);
		wait.forElementClickable(cancelReportSelectionCriteria);
		cancelReportSelectionCriteria.click();
	}
	public static void clickOnVehicleTab() throws InterruptedException
	{
		Thread.sleep(5000);
		wait.forElementClickable(vehiclesTabInReport);
		vehiclesTabInReport.click();
		
	}
	public static void clickOnDriverTab() throws InterruptedException
	{
		Thread.sleep(2000);
		wait.forElementClickable(driversTab);
		driversTab.click();
		
	}
	public static void verifyDriverEventDetailProtocolRepots() throws IOException, InterruptedException
	{
		if(driverEventDetailsProtocol.isEmpty())
		{
			Assert.assertTrue(false);
		}
		else
		{		
			Thread.sleep(3000);
			test.log(Status.PASS, "Driver Shift Protocol Visible" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "eventProtocol reports")));
		
		}
	}
	
	
	
	
}

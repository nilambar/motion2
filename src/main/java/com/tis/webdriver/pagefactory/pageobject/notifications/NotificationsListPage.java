package com.tis.webdriver.pagefactory.pageobject.notifications;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.jsoup.Connection.Method;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;

public class NotificationsListPage extends TisBasePageObject
{
	
	public NotificationsListPage(WebDriver driver)
	{
		super();
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css = "div[aria-labelledby='dropdownMenuButton3']>a")
	static List<WebElement> notifications1;
	//By notifications = By.cssSelector("div[aria-labelledby='dropdownMenuButton3']>a");

	  public static boolean isNotificationTimeVisible(String notificationText) {
	    List<WebElement>notifications = driver.findElements(By.cssSelector("div[aria-labelledby='dropdownMenuButton3']>a"));
		 if(notifications.isEmpty())
		 {
			 test.log(Status.INFO, "No notifications available for the selected criteria");
		 }
		 else
		 {
			 for (WebElement element : notifications)
			    {
			      if (element.getText().contains(notificationText))
			      {
			    		 // element.findElement(By.cssSelector("p")).getText().contains(notificationText)) {
			    	  element.click();
			    	  System.out.println("Clicked :  "+notificationText);
			    	  return true;
			      }
			    }

		 }
	    
	    return false;
	  }
	  
	  
	  public static void getTheNotificationDates() throws ParseException
	  {
		  List<WebElement>notificationDates=driver.findElements(By.cssSelector(".float-xs-right.pl-0-5"));
		  if(notificationDates.isEmpty())
		  {
			  test.log(Status.INFO, "No notifications available for the selected criteria");
		  }
		  else
		  {
			  for(WebElement element:notificationDates)
			  {
				  
				  
				  String nDate = element.getText();
				  System.out.println(nDate);
				  
				  Date date1=new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa").parse(nDate);
				  System.out.println(nDate+"\t"+date1);
				  
			  }
		  }
		  
	  }
	  
		public static void yearMonth()
		{
			YearMonth thisMonth    = YearMonth.now();
			YearMonth lastMonth    = thisMonth.minusMonths(1);
			YearMonth twoMonthsAgo = thisMonth.minusMonths(2);
	 
			DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MMMM yyyy");
	 
			System.out.printf("         Today: %s\n", thisMonth.format(monthYearFormatter));
			System.out.printf("    Last Month: %s\n", lastMonth.format(monthYearFormatter));
			System.out.printf("Two Months Ago: %s\n", twoMonthsAgo.format(monthYearFormatter));
		}
		
		public static void getTheDate() throws ParseException
		{
			String sDate1="31/12/1998";
			Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
			System.out.println(sDate1+"\t"+date1);
		}
		public static  String getTheCurrentMonth()
		{
			DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MMM yyyy");
			YearMonth month    = YearMonth.now();
			String thisMonth = month.format(monthYearFormatter);
			System.out.println(thisMonth);
			return thisMonth;
			
		}
		public static String getTheLastMonth()
		{
			DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MMM yyyy");
			YearMonth thisMonth    = YearMonth.now();
			YearMonth lMonth= thisMonth.minusMonths(1);
			String lastMonth = lMonth.format(monthYearFormatter);
			System.out.println(lastMonth);
			return lastMonth;
			
		}
		
		
		public static void  currentWeek()
		{
			Calendar calender = Calendar.getInstance();
		     int currentday = calender.get(Calendar.DAY_OF_WEEK); 
	            
	            int firstDay = currentday-1;
	            System.out.println(currentday); 
	            
	            
	            calender.add(Calendar.DATE, -firstDay); 
	            System.out.println("Current Week First date = "+ calender.getTime()); 
			
		}
		private static CharSequence getTheLastThreeMonths()
		{
			DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MMM yyyy");
			YearMonth thisMonth    = YearMonth.now();
			YearMonth twoMonthsAgo1 = thisMonth.minusMonths(2);
			String twoMonthsAgo = twoMonthsAgo1.format(monthYearFormatter);
			return twoMonthsAgo;
		}
		public static void verifyIsNotificationsInCurrentMonth() throws ParseException, IOException, InterruptedException
		{
			 List<WebElement>notificationDates=driver.findElements(By.cssSelector("div>span[class='float-xs-right']"));
			 if (notificationDates.isEmpty())
				{
					test.log(Status.INFO, "No notifications available for current Selection"+ test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Current Month Notifications")));
				}
				  
				else
				{
					 for(WebElement element:notificationDates)
					  {
						  String nDate = element.getText();
						  System.out.println("Current Month : " + nDate);
						  if(nDate.contains(NotificationsListPage.getTheCurrentMonth()))
						  {
							  test.log(Status.INFO, "This notification is in current Month : "+nDate);
							  Assert.assertTrue(true);
						  }
						  else
						  {
							  test.log(Status.INFO, "This notification is not  in current Month : "+nDate);
							  Assert.assertTrue(false);
						  }
						  
						  Date date1=new SimpleDateFormat("dd MMM yyyy, HH:mm").parse(nDate);
						  System.out.println(nDate+"\t"+date1);
						  
					  }
					 Thread.sleep(2000);
					 test.log(Status.INFO, "This notification is in Last   Month : "+ test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Current Month Notifications")));
				}
			 
			
		}
		public static void verifyIsNotificationsInLastMonth() throws ParseException, IOException, InterruptedException
		{
			List<WebElement>notificationDates=driver.findElements(By.cssSelector("div>span[class='float-xs-right']"));
			  if (notificationDates.isEmpty())
			  {
				  test.log(Status.INFO, "Notification is not present for current selection: "+ test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Last Month Notifications")));
			  }
			  else
			  {
				  for(WebElement element:notificationDates)
				  {
					  String nDate = element.getText();
					  System.out.println(nDate);
					  if(nDate.contains(NotificationsListPage.getTheLastMonth()))
					  {
						  test.log(Status.INFO, "This notification is in Last   Month : "+nDate);
						  Assert.assertTrue(true);
					  }
					  else
					  {
						  test.log(Status.INFO, "This notification is not  in Last Month : "+nDate);
						  Assert.assertTrue(false);
					  }
					  
					  Date date1=new SimpleDateFormat("dd MMM yyyy, HH:mm").parse(nDate);
					  System.out.println(nDate+"\t"+date1);
					  
				  }
				  Thread.sleep(2000);
				  test.log(Status.INFO, "This notification is in Last   Month : "+ test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Last Months Notifications")));
			  }
			  
			  
			
		}
		public static void verifyIsNotificationsInLastThreeMonths() throws ParseException, IOException, InterruptedException
		{
			List<WebElement>notificationDates=driver.findElements(By.cssSelector("div>span[class='float-xs-right']"));
			if (notificationDates.isEmpty())
			{
				test.log(Status.INFO, "No notifications available for current Selection"+ test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Last three months Notifications")));
			}
			  
			else
			{
				for(WebElement element:notificationDates)
				  {
					  String nDate = element.getText();
					  System.out.println(nDate);
					  if(nDate.contains(NotificationsListPage.getTheLastThreeMonths())||nDate.contains( NotificationsListPage.getTheLastMonth())||nDate.contains(NotificationsListPage.getTheCurrentMonth()) )
					  {
						  test.log(Status.INFO, "This notification is in Last three Months : "+nDate);
						  Assert.assertTrue(true);
					  }
					  else
					  {
						  test.log(Status.INFO, "This notification is not  in Last three Months : "+nDate);
						  Assert.assertTrue(false);
					  }
					  
					  Date date1=new SimpleDateFormat("dd MMM yyyy, HH:mm").parse(nDate);
					  System.out.println(nDate+"\t"+date1);
					  
				  }
				
				test.log(Status.INFO, "This notification is in Last three  Months : "+ test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "test")));
			}
			
			
		}
		
		public static int getEndDateOfLastWeek()
		{
			Calendar calender = Calendar.getInstance(); 
		   
            int currentday = calender.get(Calendar.DAY_OF_WEEK); 
                  
           // System.out.println(currentday); 
            
            calender.add(Calendar.DATE, -currentday); 
            int eDate = calender.get(Calendar.DATE);
           // String endDate = calender.getTime();
            
           // System.out.println("The Date is :::::::::::" +eDate);
            
          //  System.out.println("Previous Week End Date = "+ calender.getTime());
            return eDate;
            
 		}

		public static int getStartDateOfLastWeek()
		{
			Calendar calender = Calendar.getInstance(); 
	        int currentday = calender.get(Calendar.DAY_OF_WEEK); 
           // System.out.println(currentday);         
            calender.add(Calendar.DATE, -6); 
           // System.out.println("Previous Week Start Date = "+ calender.getTime());  
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss"); 
           // System.out.println(currentday);         
            calender.add(Calendar.DATE, -currentday); 
          //  System.out.println("Previous Week End Date = "+ calender.getTime()); 
         
            Date date123 = calender.getTime();
            String reportDate = df.format(date123);
            String[] lastDate=reportDate.split("/");
            int lDate = Integer.parseInt(lastDate[1]);
            calender.add(Calendar.DATE, -6); 
            int sDate = calender.get(Calendar.DATE);   
       
            
            return lDate;
		}
		
		
		public static void verifyIsNotificationsInLastWeek() throws IOException, InterruptedException
		{
			List<WebElement>notificationDates=driver.findElements(By.cssSelector("div>span[class='float-xs-right']"));
			if (notificationDates.isEmpty())
			{
				test.log(Status.INFO, "No notifications available for current Selection"+ test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Last Week Notifications")));
			}
			else
			{
				for(WebElement element:notificationDates)
				  {
					  String nDate = element.getText();
					  String[] nTime = nDate.split(" ");
					  int time = Integer.parseInt(nTime[0]);
					  
				  
					  System.out.println(nDate);
					  if(NotificationsListPage.getStartDateOfLastWeek()<=time &&  NotificationsListPage.getEndDateOfLastWeek()>=time)
					  {
						  test.log(Status.INFO, "This notification is in Last week : "+nDate );
						  Assert.assertTrue(true);
					  }
					  else
					  {
						  test.log(Status.INFO, "This notification is not  in Last week : "+nDate);
						  Assert.assertTrue(false);
					  }
						  
				  }
				Thread.sleep(2000);
				test.log(Status.INFO, "This notification is in Last   Month : "+ test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Last Week Notifications")));
			}
		
		}
		
		public void date()
		{
			 Calendar calender = Calendar.getInstance(); 
             
             int currentday = calender.get(Calendar.DAY_OF_WEEK);            
            // System.out.println(currentday);            
             calender.add(Calendar.DATE, -currentday); 
             //System.out.println("Previous Week End Date = "+ calender.getTime()); 
             
             calender.add(Calendar.DATE, -6); 
            // System.out.println("Previous Week Start Date = "+ calender.getTime());

		}
		public static int getFirstDateOfCurrentWeek()
		{
			Calendar calender = Calendar.getInstance(); 
         
            int currentday = calender.get(Calendar.DAY_OF_WEEK); 
            //System.out.println("The date is {{{{{{{{{{{{ " + currentday);
            calender.add(Calendar.DATE, -currentday+1); 
          //  System.out.println("Current WeeK Start Date = "+ calender.getTime());
            int sDate = calender.get(Calendar.DATE);
           // System.out.println("Current WeeK Start Date++++++++++++ = "+ sDate);
            return sDate;
            
            
		}
		public static int getcurrentDate()
		{
			Calendar calender = Calendar.getInstance(); 
			int cDate = calender.get(Calendar.DATE);
			//System.out.println("todays date is +++++++++++++"+cDate);
			return cDate;
			
		}
		public static void verifyIsNotificationsInCurrentWeek() throws IOException, InterruptedException
		{
			List<WebElement>notificationDates=driver.findElements(By.cssSelector("div>span[class='float-xs-right']"));
			  
			if( notificationDates.isEmpty())
			{
				test.log(Status.INFO, "No notifications available for current Selection"+ test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Current Week Notifications")));
			}
			else
			{
				
			
			for(WebElement element:notificationDates)
			  {
				  String nDate = element.getText();
				  String[] nTime = nDate.split(" ");
				  int time = Integer.parseInt(nTime[0]);					  
				 // System.out.println(nDate);
				  if(NotificationsListPage.getFirstDateOfCurrentWeek()<=time &&  NotificationsListPage.getcurrentDate()>=time)
				  {
					  test.log(Status.INFO, "This notification is in Current  week : "+nDate);
					  Assert.assertTrue(true);
				  }
				  else
				  {
					  test.log(Status.INFO, "This notification is not  in Current week : "+nDate);
					  Assert.assertTrue(false);
				  }
					  
			  }
			Thread.sleep(2000);
			test.log(Status.INFO, "This notification is in Last   Month : "+ test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Current Week Notifications")));
			}
			
		}
	

	
}

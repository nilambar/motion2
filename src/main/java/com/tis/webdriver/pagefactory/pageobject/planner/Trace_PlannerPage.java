package com.tis.webdriver.pagefactory.pageobject.planner;



import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;


/**
 * @author A405520
 *
 */
public class Trace_PlannerPage extends TisBasePageObject
{

	@FindBy(css="div>div.H_ctl>div[title='Choose view']")
	static WebElement poligonIcon;
	@FindBy(css="input[name='daterangeInput']")
	static WebElement dateRangeInput;
	@FindBy(css="#dropdownMenuButton1")
	static WebElement allSitesDropMenu;
	@FindBy(css="#dropdownMenuButton3")
	static WebElement allVehicles;
	@FindBy(css="#dropdownMenuButton2")
	static WebElement allDrivers;
	@FindBy(css=".content.datepickerInputBox")
	static WebElement clickOnDateTextBox;
	@FindBy(css=".cancelBtn.btn.btn-sm.btn-default")
	static WebElement cancelDatePicker;
	//@FindBy(css="td[class='today active start-date active end-date available']")
	@FindBy(css=".content.datepickerInputBox")
	static WebElement getTheStartDateRange;
	@FindBy(css="span[class='text-vdo-blue mr-20px']")
	static WebElement slideShiftDetail;
	@FindBy(css="div[class='align-middle-end pointer shiftTraceRight mr-25px text-vdo-blue']>span")
	static WebElement mapViewBttn;
	@FindBy(xpath="html/body/div[2]/div[2]/div[2]/table/tbody/tr[5]/td[5]")
	static WebElement startDate;
	@FindBy(css=".calendar.left>.calendar-table>table>thead>tr>.month")
	static WebElement getTheMonthYear;
	@FindBy(css=".text-uppercase.list-inline-item.TraceVEHICLES.traceListBtn.active-border")
	static WebElement vehicleTab;
	@FindBy(css="#SliderbuttonBtn")
	static WebElement closeTheSliderButton;
	@FindBy(css=".glyphs.p-0.blue-dark.font-weight-bold.mr-3.font-xlarge")
	static WebElement clickOnSearch;
	@FindBy(xpath=".//*[@id='serchWrapper']/ng2-completer/div/input")
	static WebElement searchTextBox;
	@FindBy(css=".completer-dropdown")
	static WebElement searchTable;
	@FindBy(css="span[class='mr-auto txt-overflow']")
	static List<WebElement> driverName;
	
	static By driversInList = By.cssSelector("span[class='p-s-list-fb-200px mr-auto']");
	static By totalDriversInList=By.cssSelector("div[aria-labelledby='dropdownMenuButton2']>a");
	/**dummy cssSelector taken until element available under vehicle Asset */
	static By vehicleListsInAsset = By.cssSelector("span[class='d-inline-block vehicle-status-title p-s-list-fb-200px vrnStyle mr-auto']");
	static By searchValues = By.cssSelector(".completer-dropdown");
	
	public Trace_PlannerPage(WebDriver driver) 
	{
		super();
	}
	
	public Trace_PlannerPage veriFyDefatulMainMapView() throws IOException
	{
		wait.forElementVisible(poligonIcon);
		Assert.assertTrue(poligonIcon.isDisplayed());
		test.log(Status.INFO, "Map Default Page Loaded: " +test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "MapLoad")));
		return this;
	}
	public Trace_PlannerPage getTheDefaultDate()
	{
		wait.forElementVisible(dateRangeInput);
		String defaultDate= dateRangeInput.getText();
		System.out.println(defaultDate);
		return this;
	}
	public static void verifyAvailableSitesInDropMenu() throws InterruptedException
	{
		int tSitesInDropDown = 1;
		Thread.sleep(3000);
		if (allSitesDropMenu.isDisplayed())
		{
			allSitesDropMenu.click();
			Thread.sleep(2000);
			List<WebElement> totalElements = driver.findElements(By.cssSelector(".dropdown-menu.top-blue-list[aria-labelledby='dropdownMenuButton1']>a"));
			System.out.println(totalElements);
			for (WebElement ele:totalElements)
			{
				String site = ele.getText();
				System.out.println( site);
				test.log(Status.PASS, site)	;
				tSitesInDropDown=tSitesInDropDown+1;
			}
			Assert.assertTrue(true);
			if(tSitesInDropDown==1)
			{
				test.log(Status.INFO, "No vehicles avaialbe for the current Selection");
			}
		}
	}
	
	public static List<WebElement>  verifyAvailableVehiclesInDropMenu() throws InterruptedException
	{
		ArrayList<String> vehicleList = new ArrayList<String>();
		List<WebElement> totalElements=new ArrayList<WebElement>();
		int tVehiclesInDropDown = 1;
		Thread.sleep(3000);
		if (allVehicles.isDisplayed())
		{
			allVehicles.click();
			Thread.sleep(2000);
			totalElements  = driver.findElements(By.cssSelector(".dropdown-menu.top-blue-list[aria-labelledby='dropdownMenuButton3']>a"));
			for (WebElement ele:totalElements)
			{
				String tVehicle = ele.getText();
				vehicleList.add(tVehicle);
				test.log(Status.PASS, tVehicle)	;
				System.out.println(tVehicle);
				tVehiclesInDropDown = tVehiclesInDropDown+1;
			}
			Assert.assertTrue(true);
			if(totalElements.size()==1)
			{
				test.log(Status.INFO, "No Vehicles available for the current selection");
			}

		}
		return totalElements;
		
	}
	
	/**Find the total Drivers available in the list*/
	public static List<WebElement> totalDriversInDropDown() throws InterruptedException
	{
		wait.forElementVisible(allDrivers);
		allDrivers.click();
		Thread.sleep(2000);
		List<WebElement> totalElements=new ArrayList<WebElement>();
		Thread.sleep(2000);
		totalElements=driver.findElements(totalDriversInList);
		Thread.sleep(2000);
		System.out.println(totalElements);
		int nCount=0;
		Thread.sleep(2000);
		ArrayList<String> driverList = new ArrayList<String>();
		for (WebElement ele:totalElements)
		{
			String tDrivers = ele.getText();
			driverList.add(tDrivers);
			
			System.out.println("The driver name is  :" +tDrivers);
			test.log(Status.PASS, tDrivers)	;
			Assert.assertTrue(true);
			}
			
			if(nCount==1)
			{
				test.log(Status.INFO, "No Drivers available for the current selection");
			}
			nCount=driverList.size();
			return(totalElements);
			
			
	}
	
	public static void clickOnDateTextBox() throws InterruptedException
	{
		
		Thread.sleep(5000);
		wait.forElementVisible(clickOnDateTextBox);
		clickOnDateTextBox.click();
		test.log(Status.INFO, "date text box clicked");
	}
	public static void getTheDateRange() throws ParseException
	{
	//	wait.forElementVisible(getTheStartDateRange);
		String sDate = getTheStartDateRange.getAttribute("placeholder");
		
		
		//String sMonthYear=getTheMonthYear.getText();
		System.out.println(sDate);
		//System.out.println(sMonthYear);
		
        LocalDate localDate = LocalDate.now();
        System.out.println(DateTimeFormatter.ofPattern("dd/MM/yyyy").format(localDate));
         
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
        
    //   Date date = (Date) dateFormat.parse(sDate);
        System.out.println("Today's date is "+dateFormat.format(cal.getTime()));

       cal.add(Calendar.DATE, -1);
        System.out.println("Yesterday's date was "+dateFormat.format(cal.getTime())); 
      String  date2= dateFormat.format(cal.getTime());
      
      if (sDate.contentEquals(date2))
      {
    	  test.log(Status.INFO, "Pass");
      }
      else
      {
    	  test.log(Status.FAIL, "failed");
      }
      
        
        
	}

	public static int getAllDriversInSliders()
	{
		int driversCount =0;
		String dName=null;
		if(driverName.isEmpty())
		{
			test.log(Status.INFO, "No drivers available for the current Selection");
		}
		else
		{
			//wait.forElementVisible(driverName);
			List<WebElement>drivers=driver.findElements(driversInList);
			for(WebElement ele:drivers)
			{
				dName=ele.getText();
				driversCount = driversCount +1;
				System.out.println(dName);
			}
			System.out.println("Total Drivers:  "+driversCount);
			
		}
	
		return driversCount;
		//wait.forElementVisible(driverName);
		

	}
	public static void  clickShiftDetail()
	{
		wait.forElementVisible(slideShiftDetail);
		slideShiftDetail.click();
		test.log(Status.INFO, "Shift detail button Clicked");
	}
	public static void verifyShiftDetails()
	{
		wait.forElementVisible(mapViewBttn);
		Assert.assertTrue(true);
		test.log(Status.PASS, "Shift Details Page Dispalyed");
	}
	public static void clickOnAssetHideBttn(By sliderButton)
	{
		wait.forElementVisible(slideShiftDetail);
		driver.findElement(sliderButton).click();
		
	}
	public static void clickOnMapView()
	{
		wait.forElementVisible(mapViewBttn);
		mapViewBttn.click();
		test.log(Status.INFO, "Map View Clicked");
		
	}
	public static void verifyIsAssetHide()
	{
		if (slideShiftDetail.isDisplayed())
		{
			Assert.assertTrue(false);
			
		}
		else
		{
			Assert.assertTrue(true);
			test.log(Status.INFO, "Asset Hides successfuly");
		}
	}
	
	/**Select a single Driver from the driver drop down in trace page*/
	public static void selectDriver(int indx) throws InterruptedException
	{
		Trace_PlannerPage.totalDriversInDropDown().get(2).click();

	}

	public static void selectVehicle(int i) throws InterruptedException, IOException
	{
		if(Trace_PlannerPage.verifyAvailableVehiclesInDropMenu().isEmpty())
		{
			test.log(Status.INFO, "No Vehicles Available for the current Selecion" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "vehiclesInAssets")));
		}
		else
		{
			Trace_PlannerPage.verifyAvailableVehiclesInDropMenu().get(i).click();
		}
		
		
		
	}
	public static void clickOnVehiclesTabInAsset() throws InterruptedException
	{
		Thread.sleep(2000);
	//	wait.forElementVisible(vehicleTab);

		//	vehicleTab.click();
		driver.findElement(By.cssSelector(".text-uppercase.list-inline-item.TraceVEHICLES.traceListBtn")).click();;
			test.log(Status.INFO, "Vehicle Tab Clicked");

	}

	public static void verifyVehicleInAsset(int i) throws InterruptedException
	{
		
		String aVehicle = driver.findElement(By.cssSelector("#dropdownMenuButton3")).getText();
		List<WebElement> vehicleLists=driver.findElements(vehicleListsInAsset);
		
		
		if (vehicleLists.isEmpty())
		{
			Assert.assertTrue(false , "Vehicle shoule available in Vehicle Asset");
		}
		else
		{
			for(WebElement ele:vehicleLists)
			{
					
				if (ele.getText()==aVehicle)
				{
					Assert.assertTrue(true);
					test.log(Status.PASS, "Selected Vehicle Available in List");
					break;		
				
				}
				else
				{
					Assert.assertTrue(false, "No vehicle displayed for the selection");
				}
			}
		}
		
	}
	public static List<WebElement> getTheTraceForVehicles()
	{
		List<WebElement>elementLists=new ArrayList<WebElement>();
		
		List<WebElement>routeInMap=driver.findElements(By.cssSelector(".glyphs.p-0.font-150"));
		if(routeInMap!=null)
		{
			for(WebElement ele:routeInMap)
			{
				elementLists.add(ele);
			}
			
		}
		else
		{
			test.log(Status.INFO, "No Veichle trace is available");
		}
		return elementLists;
	}
	public static void verifyTheTrace(int i) throws IOException, InterruptedException
	{
		WebElement traceIcon =Trace_PlannerPage.getTheTraceForVehicles().get(i);
		Thread.sleep(4000);
		if (traceIcon.isDisplayed())
		{
			
			traceIcon.click();
			Thread.sleep(3000);
			test.log(Status.INFO, "Trace Generated:" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "MapLoad")));
		}
		
		
	}
	public static void clickOnCloseSliderBttn()
	{
		wait.forElementVisible(closeTheSliderButton);
		closeTheSliderButton.click();
		test.log(Status.INFO, "Slider Button Closed");
	
	}

	public static void clickOnSearch()
	{
		wait.forElementVisible(clickOnSearch);
		clickOnSearch.click();
		test.log(Status.INFO, "Search Icon Clicked");
		
	}
	public static void enterSearchText()
	{
		wait.forElementVisible(searchTextBox);
		searchTextBox.sendKeys("ien");
		test.log(Status.INFO, "Search text Entered ");
	}
	
	public static List<WebElement> gettingSearchValue()
	{
		List<WebElement>results=driver.findElements(searchValues);
		for(WebElement ele:results)
		{
			ele.click();
			String name = ele.getText();
			System.out.println(name);
		}
		return results;
		
	}

	public static void cancelCalendar() throws InterruptedException
	{
		Thread.sleep(2000);
		wait.forElementClickable(cancelDatePicker);
		cancelDatePicker.click();
		
	}

}

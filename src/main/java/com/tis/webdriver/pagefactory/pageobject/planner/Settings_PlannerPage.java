package com.tis.webdriver.pagefactory.pageobject.planner;



import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;

public class Settings_PlannerPage extends TisBasePageObject
{
	//@FindBy (xpath = "//input[@id = 'prio1'][@value = 'All sites']")
	@FindBy (css=".form-group.radio.radio-primary.ml-1>input[id='prio1']")
	static WebElement allSitesCheckbox;
	@FindBy (xpath = ".//*[@id='page-wrapper']/app-setting/div[4]/div[3]/div[2]/div")
	static WebElement sitesCheckbox;
	
	private static boolean isChecked;
	
	public Settings_PlannerPage(WebDriver driver)
	{
		super();
		PageFactory.initElements(driver, this);
		
	}

	public static  void verifyAllSiteCheckboxChecked() throws IOException
	{
			
		//wait.forElementVisible(allSitesCheckbox);
		
		isChecked = allSitesCheckbox.isSelected();
		if (isChecked == true)
		{
			System.out.println("PASS");
			Assert.assertTrue(true);
			test.log(Status.PASS,  "All Sites Radio Button default selected  :  " +test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "allSitesChecked")));
		}
		else
		{
			System.out.println("Fail");
			Assert.assertTrue(false);
		}
		
	}
	
	
	
	public static void verifySitesCheckBox() throws InterruptedException, IOException
	{
		
		//wait.forElementClickable(sitesCheckbox);
		
		isChecked = sitesCheckbox.isSelected();
		if (isChecked == false) 
		{
			sitesCheckbox.click();
			Thread.sleep(2000);
			boolean isEnabled = driver.findElement(By.xpath(".//*[@id='setting-site-name-reference']")).isEnabled();
			if (isEnabled == true)
			{
				test.log(Status.PASS, "The test box is enabled");
			}
			else
			{
				Assert.assertTrue(false, "Text box is not enabled");;
			}
			Thread.sleep(4000);
			WebElement radiusGroup = driver.findElement(By.xpath("//div[@class='form-group flex mb-0']"));
			if (radiusGroup.isDisplayed())
			{
				String radiusValue = radiusGroup.getText();
			
			
			
			//String radiusValue = driver.findElement(By.cssSelector("#formGroup")).getText();
				System.out.println(radiusValue);
				if (radiusValue.contentEquals("30"))
				{
					Assert.assertTrue(true);
					test.log(Status.PASS, "default value 30 displayed :  " +test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "allSitesChecked")));
				}
				else
				{
					Assert.assertTrue(false, "The default value 30 should display");
				}
			
			}
		}
		
	}

	public static void clickSiteRadio() throws InterruptedException
	{
		Thread.sleep(4000);
		if (!sitesCheckbox.isSelected())
		{
			sitesCheckbox.click();
		}
	}
	
	public static void selectSitesFromDropDown(String site) throws InterruptedException
	{
	Thread.sleep(1000);
		
		driver.findElement(By.xpath(".//*[@id='setting-site-name-reference']")).click();
		List<WebElement> combo= driver.findElements(By.xpath(".//*[@id='setting-site-name-reference']/option"));
		int cSize=combo.size();	
		//for (WebElement ele :combo)
		for (int i = 0; i <= cSize; ++i)
		{
			
			WebElement elementName = combo.get(i);
			
			String siteName=elementName.getText();
					System.out.println(siteName);
			
			if (siteName.contentEquals(site))
			{
				Thread.sleep(3000);
				elementName.click();
				elementName.sendKeys(Keys.ENTER);
				Thread.sleep(3000);

				test.log(Status.INFO, "The site Selected: "+site);
				break;
			}

		}
			
	}

	public static void saveSitesData(String site) throws InterruptedException, IOException 
	{
		Settings_PlannerPage.selectSitesFromDropDown(site);
		driver.findElement(By.xpath(".//*[@id='formGroup']")).clear();
		driver.findElement(By.xpath(".//*[@id='formGroup']")).sendKeys("30");
		driver.findElement(By.xpath(".//*[@id='page-wrapper']/app-setting/div[3]/div/input")).click();
		Thread.sleep(2000);
		String successMessage= driver.findElement(By.xpath(".//*[@id='page-wrapper']/app-setting/div[4]/div[1]")).getText(); 
		
		System.out.println(successMessage);
		if (successMessage.contains("Setting saved successfully"))
		{
			Assert.assertTrue(true, "Site updated");
			test.log(Status.PASS, "Added site saved successfully : " +test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "site saved")));
		}
		else
		{
			Assert.assertTrue(false, "Site should save successfully");;
		}
		
	}
}

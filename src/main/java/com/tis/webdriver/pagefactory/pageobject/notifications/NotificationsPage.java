package com.tis.webdriver.pagefactory.pageobject.notifications;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.utility.SyncUtility;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;



public class NotificationsPage extends TisBasePageObject
{
	@FindBy (xpath = ".//*[@id='section-title']")
	static	WebElement notificationTitle;
	@FindBy (xpath = "//*[contains(@id,'notifications-list')]")
	static WebElement notifications;
	@FindBy (xpath = ".//*[@id='NotificationActive']")
	static WebElement activeNotificationTab;
	@FindBy (css = "#NotificationCompleted")
	static WebElement completedNotificationTab;
	@FindBy (xpath = ".//*[@id='NotificationsList']/app-notification-active-side-list/div/div/div[4]/div[1]/button")
	static	WebElement clickAllNotifications;
	//@FindBy (xpath = "//span[@class='pl-1']")
	
	//static WebElement severityCritical;
	@FindBy(xpath="//div[@class='pl-0 small text-nowrap flex ml-auto']")
	static List<WebElement> severityType;
	
	
	@FindBy (xpath = "//span[@class='pl-2'][i[@class='fa fa-circle pr-0-5 fa-orange-circle']]")
	static WebElement severityHigh;
	@FindBy (xpath = "//span[@class='pl-2'][i[@class = 'fa fa-circle pr-0-5 fa-gold-circle']]")
	static WebElement severityMedium;
	@FindBy (xpath = "//span[@class='pl-2'][i[@class = 'fa fa-circle pr-0-5 fa-grey-circle']]")
	static WebElement severityLow;
	@FindBy (xpath = "//button/span[contains(text(),'All Notifications')]")
	static WebElement allNotifications;
	@FindBy (css = ".checkbox.checkbox-primary")
	static WebElement unReadCheckBox;
	//@FindBy (xpath=".//*[@id='ActiveScreen']/div[1]/div/div[2]/span/span[1]/span/button")
	@FindBy(xpath ="//button[@class='btn btn-secondary blue-button']/span[contains(text(),'More')]")
	static WebElement moreButton;
	//@FindBy (xpath=".//*[@id='ActiveScreen']/div[1]/div/div[2]/span/span[1]/button")
	@FindBy (xpath=".//*[@id='ActiveScreen']/div[1]/div/div[2]/span/span[1]/button")
	static WebElement setAsDoneButton;
	@FindBy (xpath=".//*[@id='notifications']/app-notification-dropdownmenu/div/div[1]/div[1]/div[1]")
	static WebElement timeFilter;
	@FindBy (xpath="//button[@id='dropdownMenuButton3']")
	static List<WebElement> filter;
	
	//@FindBy (xpath = ".//*[@id='notifications']/app-notification-dropdownmenu/div/div[1]/div[1]/div[2]")
	@FindBy(xpath="//span[@title='Sort By Date']")
	static WebElement sortFilter;
	@FindBy (xpath = ".//*[@id='NotificationsList']/app-notifications-completed-list-details/div/div[1]/div/div[1]/div[2]/span/button")
	static WebElement bttnReOpen;
	@FindBy(css="div[id='ActiveScreen']>div>div>div>span[class='pl-0-md pr-0-md notificarionsName font-weight-bold']")
	static WebElement notificationCompletedInActiveScreen1;
	@FindBy(css="#NotificationActive")
	static WebElement activeTab;
	@FindBy(css=".glyphs.p-0.blue-dark.font-weight-bold.font-large")
	static WebElement searchIcon;
	@FindBy(xpath=".//*[@id='NotificationList']/div/a/div[1]/div[2]/span[1]")
	static WebElement driverOrVRNFromList;
	@FindBy(xpath=".//*[@id='activeSearchTextBox']")
	static WebElement searchTextBox;
	@FindBy(xpath="//span[@class='col-sm-5 p-0 NSLTitle']")
	static List<WebElement> listElement;
	
	@FindBy (xpath = ".//*[@id='NotificationsList']/app-notification-completed-side-list/div/div/div[4]/div[1]/button")
	static WebElement clickNotificationsInCompleted;
	
	@FindBy (xpath="//div[@class='pl-0 small text-nowrap flex ml-auto']")
	static List<WebElement> sevList;
	@FindBy (xpath = ".//*[@id='create_geo_btn']")
	static List<WebElement> confirmBttn;
	
	
	
	
	
	static By allNotificationElementsInActiveTab = By.cssSelector(".row.border-bottom-grey.pt-1.pb-1.notificationsInactive");

	
	static By valuesInTimeFilter = By.xpath("//div[@aria-labelledby='dropdownMenuButton3']/a");
	static By valueInSortFilter = By.xpath(".//*[@id='notifications']/app-notification-dropdownmenu/div/div[1]/div[1]/div[2]/div/a");
	
	static By activeNotification = By.cssSelector("#NotificationActive");
	static By completedNotification = By.cssSelector("#NotificationCompleted");
	static By notificationCompletedInActiveScreen = By.cssSelector("div[id='ActiveScreen']>div>div>div>span[class='pl-0-md pr-0-md notificarionsName font-weight-bold']");
	static By notificationsActive=By.cssSelector(".pl-0-md.pr-0-md.notificarionsName.font-weight-bold");
	static By allNotificationElementsInCompletedTab = By.cssSelector(".row.border-bottom-grey.pt-1.pb-1.notificationsInactive");
	
		
	static int totalUnreadNotifications;
	int totalCriticalNotifications;
	int totalHighNotifications;
	int totalMediuNotification;
	int totalLowNotification;
	static int totalNotifications;
	
	public NotificationsPage(WebDriver driver)
	{
		super();
	}

	public static boolean openActiveNotificatons()
	{
		if(activeNotificationTab.isDisplayed())
		{
			activeNotificationTab.click();
			test.log(Status.INFO, "Active Notification Tab has clicked");
			return true;
		}
		else
		{
			test.log(Status.INFO, "Active Notification not visisble");
			return false;
		}
	}
	
	public NotificationsPage openCompletedNotification()
	{
		wait.forElementClickable(completedNotificationTab);
		completedNotificationTab.click();
		
		return this;
	}
	
	public int unReadNotifications()
	{
		wait.forElementVisible(unReadCheckBox);
		unReadCheckBox.click();
		List<WebElement> noOfCheckbox = driver.findElements(By.cssSelector("#NotificationList [type = 'checkbox']"));
		totalUnreadNotifications = noOfCheckbox.size();
		return totalUnreadNotifications;
	}
	
	public static ArrayList<String> notificationSeverity() throws InterruptedException
	{
		//int totalCriticalNotifications = 0;		
		
		List<WebElement> ele = new ArrayList<WebElement>();
		
	 ele = driver.findElements(By.xpath("//div[@class='pl-0 small text-nowrap flex ml-auto']"));
	//	String ele2 =driver.findElements(By.xpath("//div[@class='pl-0 small text-nowrap flex ml-auto']/span")).get(0).getText();
		
		ArrayList<String> listItem= new ArrayList<String>();
		
		if(severityType.isEmpty())
		{
			
		}
		else
		{
			for(WebElement ele1 : ele)
			{
				String totalEle = ele1.getText();
				String temp[]=totalEle.split("\\(");
				System.out.println(temp[0]);
				for (int i =1 ;i < 5 ;++i)
				{
					String test[] = temp[i].split("\\)");
					
					listItem.add(test[0]);
					
					int totalHighNotifications = Integer.parseInt(test[0]);
					System.out.println(totalHighNotifications);
					System.out.println(listItem);
					//System.out.println(test.toString().charAt(0));
				}
					
					System.out.println(totalEle.charAt(10));
				
				System.out.println(ele1.getText());
			}
		}
		return listItem;
	}
	
	public static int highNotifications()
	{
		String textItem = severityHigh.getText();

		
		String temp[]=textItem.split("\\(");
		String[] test = temp[1].split("\\)");
	
		int totalHighNotifications = Integer.parseInt(test[0]);
	
		
		return totalHighNotifications;
	}
	
	public static int mediumNotifications()
	{
		String textItem = severityMedium.getText();
		
	
		String temp[]=textItem.split("\\(");
		String[] test = temp[1].split("\\)");
	
		int totalMediuNotification = Integer.parseInt(test[0]);

		return totalMediuNotification;
	}
	
	public static int lowNotifications()
	{
		
		String textItem = severityLow.getText();

		String temp[]=textItem.split("\\(");
		String[] test = temp[1].split("\\)");
	
		int totalLowNotification = Integer.parseInt(test[0]);

		return totalLowNotification;
	}
	
	public static int totalActiveNotificationsInGrid()
	{
		//openActiveNotificatons();
		
		
		//this will be used for active notification
		List<WebElement> noOfNotifications = driver.findElements(By.xpath(".//*[@class='rightBorder']/div"));
		
		//This will be used for completed notification
		//List<WebElement> noOfNotifications = driver.findElements(By.xpath("//div[@id='NotificationList']/div"));
		
		if(noOfNotifications.isEmpty())
		{
			test.log(Status.INFO, "No Notifications available for current selection");
			totalNotifications = 0;
		}
		else
		{
			totalNotifications = noOfNotifications.size();
		}
		
		
		System.out.println("The total number of Notifications" +noOfNotifications.size());
		
		
		return totalNotifications;
	}
	public static int totalCompletedNotifications1()
	{
		//openActiveNotificatons();
		
		
		//this will be used for active notification
		//List<WebElement> noOfNotifications = driver.findElements(By.xpath(".//*[@class='rightBorder']/div"));
		
		//This will be used for completed notification
		List<WebElement> noOfNotifications = driver.findElements(By.xpath("//div[@id='NotificationList']/div"));
		
		if(noOfNotifications.isEmpty())
		{
			test.log(Status.INFO, "No Notifications available for current selection");
			totalNotifications = 0;
		}
		else
		{
			totalNotifications = noOfNotifications.size();
		}
		
		
		System.out.println("The total number of Notifications" +noOfNotifications.size());
		
		
		return totalNotifications;
	}
	
	public static int totalCompletedNotificationsInGrid()
	{
		new NotificationsPage(driver).openCompletedNotification();
		
		List<WebElement> noOfCheckbox = driver.findElements(By.cssSelector("#NotificationList [type = 'checkbox']"));
		
		System.out.println("The total number of of Checkbox" +noOfCheckbox.size());
		
		totalNotifications = noOfCheckbox.size();
		return totalNotifications;
	}
	
	
	
	public static int severityNotificationsFromCombo(String severityType) throws InterruptedException
	{
		
		int totalNotifications =  NotificationsPage.totalActiveNotificationsInGrid();
		
		return totalNotifications;
	}
	public static int completedSeverityNotificationsFromCombo(String severityType) throws InterruptedException
	{
		
		int totalNotifications =  NotificationsPage.totalActiveNotificationsInGrid();
		
		return totalNotifications;
	}
	
	public static void clickOnSeverityNotifiatons(String severityType) throws InterruptedException
	{
		Thread.sleep(2000);
		
		List<WebElement> webElement = driver.findElements(By.xpath(".//*[@id='NotificationsList']/app-notification-completed-side-list/div/div/div[4]/div[1]/button"));
		
		if(webElement.size()>0)
		{
			clickNotificationsInCompleted.click();
			
			test.log(Status.INFO, "All Notifications combobox clicked");
		}
		else
		{
			clickAllNotifications.click();
		}
		Thread.sleep(2000);
		List<WebElement> totalElements = driver.findElements(By.xpath("//div[@class = 'dropdown-menu top-blue-list custom-dropdown-list']/div/a"));
		
		for (WebElement Ele :totalElements )
		{
			String dropDownValues = Ele.getAttribute("innerHTML");
			//System.out.println(dropDownValues);
			if (dropDownValues.contentEquals(severityType))
			{
				//System.out.println(Ele.getText());
				Ele.click();
				test.log(Status.INFO, "Notification Clicked is " +severityType);
				
				break;
			}
		}
	}
	
	
	public static void verifyPageHeaderNotification()
	{
		wait.forElementVisible(notificationTitle);
			
		Assert.assertEquals(notificationTitle.getText(), "NOTIFICATIONS");
	
	}
	public static void verifyThenumberOfavailableNotifications() throws InterruptedException
	{
		
		Thread.sleep(2000);
		driver.findElement(By.xpath(".//*[@id='NotificationsList']/app-notification-active-side-list/div/div/div[4]/div[1]/button")).click();
		List<WebElement> elementsRoot = driver.findElements(By.xpath("//div[@class = 'dropdown-menu top-blue-list custom-dropdown-list']/a"));
				for(int i = 0; i < elementsRoot.size(); ++i) 
				{
									
					WebElement values = elementsRoot.get(i);
					
					String innerhtml = values.getAttribute("innerHTML");
				    
				}
				String criticalValue = driver.findElement(By.xpath(".//*[@id='page-wrapper']/app-notification-active/div[1]/div/div/div[2]/span[1]")).getText();
		
	}

	public static void criticalNotifcationsInActiveTab(String severity) throws InterruptedException 
	{
				
		NotificationsPage.clickOnSeverityNotifiatons(severity);
		
		List<WebElement> noOfCheckbox = driver.findElements(By.cssSelector("#NotificationList [type = 'checkbox']"));
	
		System.out.println("The total number of of Checkbox" +noOfCheckbox.size());
		
		noOfCheckbox.size();

	}

	public static void clickAllNotificationsFromActive()
	{
		wait.forElementClickable(activeTab);
		activeTab.click();
	}
	public  void notificationsEdit(String string)
	{
		wait.forElementVisible(moreButton);
		moreButton.click();
		List<WebElement> attributesElement = driver.findElements(By.xpath("//button[@class='dropdown-item pointer']"));
		for(WebElement ele:attributesElement)
		{
			System.out.println(ele.getText());
			while (ele.getText().contentEquals(string))
			{
				ele.click();
				test.log(Status.INFO,"Edit selected");
				break;
			}
		}
	}
	public  NotificationsPage notificationsActiveSetAsDone() throws InterruptedException
	{
		Thread.sleep(2000);
		wait.forElementVisible(setAsDoneButton);
		setAsDoneButton.click();
		
		return this;
	
	}

	public enum CurrentTime 
	{
	CurrentMonth,lastWeek,CurrentWeek,LastThreeMonths,LastMonth

	}
	
	public static int totalActiveNotifications()
	{
		wait.forElementVisible(activeNotificationTab);
		String sCount = driver.findElement(activeNotification).getText();
		System.out.println(sCount);
		String temp[]=sCount.split("\\(");
		String[] test = temp[1].split("\\)");
		int totalActiveNotification = Integer.parseInt(test[0]);
		return totalActiveNotification;

	}
	public static int totalCompletedNotifications()
	{
		wait.forElementVisible(completedNotification);
		String sCount = driver.findElement(completedNotification).getText();
		System.out.println(sCount);
		String temp[]=sCount.split("\\(");
		String[] test = temp[1].split("\\)");
		int totalCompletedNotifications = Integer.parseInt(test[0]);
		
		return totalCompletedNotifications;
	}
	public static void verifyActiveNotifications()
	{
		Assert.assertEquals(NotificationsPage.totalActiveNotifications(), NotificationsPage.totalActiveNotificationsInGrid());
		test.log(Status.INFO, "Total notification Matched");
		
	}
	public static void verifyCompletedNotifications()
	{
		Assert.assertEquals(NotificationsPage.totalCompletedNotifications1(), NotificationsPage.totalCompletedNotificationsInGrid());
		test.log(Status.INFO, "Total notification Matched");
	}
	public static List<WebElement>  getValueFromTimeFilter() throws InterruptedException
	{
		NotificationsPage.clicOnTimeFilter();
		List<WebElement> elements = new ArrayList<WebElement>();
		List<WebElement> timeFilterElements =driver.findElements(valuesInTimeFilter);
		for(WebElement ele:timeFilterElements)
		{
			elements.add(ele);
			ele.click();
			Thread.sleep(2000);
			NotificationsPage.clicOnTimeFilter();
		}
		return elements;
	}
	
	public static List<WebElement>getValueFromSortingFilter() throws InterruptedException
	{
		NotificationsPage.clickOnSortFilter();
		List<WebElement> elements1 = new ArrayList<WebElement>();
		List<WebElement> sortFilterElements =driver.findElements(valueInSortFilter);
		for(WebElement ele1:sortFilterElements)
		{
			elements1.add(ele1);
			System.out.println(ele1.getText());
			ele1.click();
			Thread.sleep(2000);
			NotificationsPage.clickOnSortFilter();
			
		}
		return elements1;
	}
	
	public static void clicOnTimeFilter() throws InterruptedException
	{
		//wait.forElementClickable(timeFilter);
		//timeFilter.click();
		Thread.sleep(3000);
		if(filter.isEmpty())
		{
			
		}
		else
		{
			System.out.println(filter.get(0).getText());
			filter.get(0).click();
			
		}
		test.log(Status.INFO,"Time Filter Clicked");
	}
	public static void clickOnSortFilter()
	{
		//wait.forElementClickable(sortFilter);
		//sortFilter.click();
		
		if(filter.isEmpty())
		{
			
		}
		else
		{
			System.out.println(filter.get(0).getText());
			filter.get(1).click();
			
		}
		test.log(Status.INFO,"Sort Filter Clicked");
	}
	public NotificationsPage  clikcOnBttnReOpen()
	{
		wait.forElementClickable(bttnReOpen);
		bttnReOpen.click();
		return this;
	}
	public NotificationsPage getVehicleInCompletedOrActiveTab()
	{
		String VRN = driver.findElement(notificationsActive).getText();
		System.out.println(VRN);
		return this;
	}

	public void verifyNotification() throws InterruptedException 
	{
		NotificationsPage notifications = new NotificationsPage(driver);
		notifications.getAllNotificationsInActiveTab();
		
	}
	public static List<WebElement> getAllNotificationsInActiveTab() throws InterruptedException
	{
		List<WebElement> allNotifications = new ArrayList<WebElement>();
		List<WebElement> element = driver.findElements(allNotificationElementsInActiveTab);
		
		
		
		//System.out.println(allNotifications.get(1).getText());
		for(WebElement ele:element)
		{
			allNotifications.add(ele);
		}
		allNotifications.get(1).click();
		Thread.sleep(2000);
		new NotificationsPage(driver).getVehicleInCompletedOrActiveTab();
		return allNotifications;
		
		
	
	}
	public  NotificationsPage getAllNotificationsInCompletedTab()
	{
		List<WebElement> allCompletedNotifications = new ArrayList<WebElement>();
		allCompletedNotifications=driver.findElements(allNotificationElementsInCompletedTab);
		System.out.println(allCompletedNotifications.get(1).getText());
		return this;
	}

	public NotificationsPage clickOnSearchIcon() 
	{
		wait.forElementClickable(searchIcon);
		searchIcon.click();
		test.log(Status.INFO, "Search Icon clicked");
		return this;
		
	}

	public static String getDnameOrVRNFromList() throws InterruptedException
	{
		String vehiclOrdName = null;
		
		Thread.sleep(7000);
	//	SyncUtility.isElementPresnt(driver, "//span[@class='col-sm-5 p-0 NSLTitle']", 20);
		
		if(listElement.isEmpty())
		{
			vehiclOrdName = "Peter"; 
		}
		else
		{
			vehiclOrdName = listElement.get(0).getText();
			System.out.println("The vehicle or driver name is : :::"+vehiclOrdName);
		}
		
		return vehiclOrdName;

	}

	public void searchDriverOrVehicle(String name) throws IOException
	{
		SyncUtility.isElementPresnt(driver, ".//*[@id='activeSearchTextBox']", 20);
	//	wait.forElementVisible(searchTextBox);
		searchTextBox.sendKeys(name);
		
		List<WebElement>elementlist = driver.findElements(By.cssSelector(".completer-item-text"));
		if(elementlist.isEmpty())
		{
			test.log(Status.INFO, "No result exist for the search :  "+name+test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "search")));
			//Assert.assertTrue(false);
		}
		else
		{
			elementlist.get(0).click();
		}
		
		//searchTextBox.click();
		//searchTextBox.sendKeys(Keys.ENTER);
		test.log(Status.INFO, "Searched With the value  :  "+name);
	}

	/**
	 * This Method is to verify the search result in the list
	 * @throws IOException */
	public void verifySearchInList(String dName) throws IOException
	{
		List<WebElement>elementLists = driver.findElements(By.xpath("//div[@class = 'col-xs-11']"));
		if(elementLists.isEmpty())
		{
			test.log(Status.INFO, "no results available for the current search" +dName+test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "search")));
			
		}
		else
		{
			if(elementLists.get(0).getText().contains(dName))
					{
					test.log(Status.INFO, "Vehicle or Driver present in the list" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "search")));	
					Assert.assertTrue(true, "Driver or Vehicle should display");
					}
			else
			{
				Assert.assertTrue(false, "Seach result is not working");
			}
				
		}
	
	}
	
	public static void sevListItems()
	{
		if(sevList.isEmpty())
		{
			System.out.println("No element available");
		}
		else
		{
			for(WebElement ele:sevList)
			{
				System.out.println(ele.getText());
			}
		}
		
	}
	
	public static void clickOnFirstDropDown()
	{
		if(filter.isEmpty())
		{
			test.log(Status.INFO, "No element available in the list");
		}
		else
		{
			System.out.println(filter.get(0).getText());
			filter.get(0).click();
			
		}
	}

	public void clickOnConfirmBttn()
	{
		if(confirmBttn.isEmpty())
		{
			
		}
		else
		{
			confirmBttn.get(0).click();
			test.log(Status.INFO, "Yes button clicked in the pop up");
		}
	
	}

	public void notificationsMarkAsUnread(String string) {
		wait.forElementVisible(moreButton);
		moreButton.click();
		List<WebElement> attributesElement = driver.findElements(By.xpath("//button[@class='dropdown-item pointer']"));
		for(WebElement ele:attributesElement)
		{
			System.out.println(ele.getText());
			while (ele.getText().contentEquals(string))
			{
				ele.click();
				test.log(Status.INFO,"Edit selected");
				break;
			}
		}
		
	}
	
}


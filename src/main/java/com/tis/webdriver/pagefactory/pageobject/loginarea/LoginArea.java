package com.tis.webdriver.pagefactory.pageobject.loginarea;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.tis.webdriver.common.core.element.Wait;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;
import com.tis.webdriver.pagefactory.pageobject.homepage.TisHomePage;



public class LoginArea extends TisBasePageObject
{
		
	@FindBy(id ="txtAccount")
	private WebElement accountField;
	
	@FindBy (id = "txtUser" )
	private WebElement userNameField;

	@FindBy (id = "txtPassword")
	private WebElement passWordField;
	
	@FindBy (id = "btnLogin")
	private WebElement loginBttn;
	
	
	private Wait wait;
	
	public LoginArea(WebDriver driver)
	{
		super();
		PageFactory.initElements(driver, this);
		this.wait = new Wait(driver);
	}
	
	public LoginArea userLogin(String account, String uName, String pWord)
	{
		wait.forElementVisible(accountField);
	
		accountField.sendKeys(account);
		userNameField.sendKeys(uName);
		passWordField.sendKeys(pWord);
		
		
		return this;
	}
	public LoginArea enterAccount(String account)
	{
		wait.forElementVisible(accountField);
		accountField.sendKeys(account);
		
		return this;
	}
	
	public LoginArea enterUserName(String uName)
	{
		wait.forElementVisible(userNameField);
		userNameField.sendKeys(uName);
		
		return this;
	}
	
	public LoginArea enterPassWord(String pWord)
	{
		wait.forElementVisible(passWordField);
		passWordField.sendKeys(pWord);
		
		return this;
	}
	
	public TisHomePage clickLoginbttnToLogin() throws InterruptedException
	{
		
		
		wait.forElementVisible(loginBttn);
		loginBttn.click();
		
		Thread.sleep(5000);
		try
		{
			
			Alert alert = driver.switchTo().alert();
			System.out.println(alert.getText());
			alert.accept();

		} catch (Exception e) {
			System.out.println("The exception catched is : " + e);
		}
		Thread.sleep(5000);
		
		return new TisHomePage(driver);
	}
}

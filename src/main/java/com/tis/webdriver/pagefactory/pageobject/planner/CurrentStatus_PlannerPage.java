package com.tis.webdriver.pagefactory.pageobject.planner;



import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.sikuli.api.robot.Key;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.core.element.CommonExpectedConditions;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;


/**
 * @author A405520
 *
 */
public class CurrentStatus_PlannerPage extends TisBasePageObject
{
	@FindBy(xpath="//li[@class='text-uppercase list-inline-item activateable section-drivers nav-item section-active' and contains(text(),'Drivers')]")
	static WebElement driverElement;
	@FindBy(xpath = "//div[@class='text-lg-right text-md-left btn-full-list']")
	static WebElement fullViewListBttn;
	@FindBy(xpath = "//span[contains(text(),'Map View')]")
	static WebElement mapViewlinnk;
	@FindBy(xpath = "//label[@class='switch']/div[@class='slider']")
	static WebElement toggleBttn;
	@FindBy(xpath="//div[@class='H_btn'][@title='Zoom in']")
	static WebElement zoomInBttn;
	@FindBy(xpath="//span[@class='p-s-list-fb-200px driverName txt-overflow']")
	static List<WebElement> totalDrivers;
	@FindBy(xpath=".//*[@id='p-s-vdo-counter-data']/app-vdo-counter/div[1]/i")
	static WebElement closeBttnDrivinghrs;
	@FindBy(xpath =".//*[@id='p-s-driver-list']/div/div[1]/div[2]/i")
	static WebElement drivinghrsIcon;
	@FindBy(xpath = ".//*[@id='btn-planner-overview']/a/span")
	static WebElement toggleSlider;
	@FindBy (xpath = "//i[@class='glyphs p-0 blue-dark font-weight-bold mr-3 font-xlarge']")
	static WebElement searchIconClick;
	@FindBy (xpath = ".//*[@id='serchWrapper']/ng2-completer/div/input")
	static WebElement searchTextbox;
	@FindBy (xpath = "//i[@class='glyphs text-vdo-red mr-3px']")
	static WebElement remainingDrivingTime;
	//span[@class='irs-min']
	
	@FindBy (xpath=".//*[@id='p-s-f-remaining-driving-time-wrapper']/div[2]/ion-range-slider/span/span[1]/span[4]")
	static WebElement leftRangeRemainigDrivingTime;
	
	@FindBy (xpath=".//*[@id='p-s-f-remaining-driving-time-wrapper']/div[2]/ion-range-slider/span/span[1]/span[5]")
	static WebElement rightRangeRemainigDrivingTime;
	//@FindBy (xpath="//*[@id='p-s-f-remaining-driving-time-wrapper']/div[2]/ion-range-slider/span/span/span[2]")
	//static WebElement leftRangeRemainigDrivingTime;
	@FindBy(xpath="//button[@class='btn btn-secondary no-border no-background noPadding']")
	static WebElement clickOnSorting;
	@FindBy(xpath="//span[@class='text-vdo-blue driver-status-title p-s-list-fb-179px pl-21px']")
	static WebElement firstelementDriving;
	@FindBy(xpath=".//*[@id='PlannerSecondCol']/ng2-completer/div/input")
	static WebElement chooseLocation;
	//@FindBy(xpath="//i[@class='fa fa-plus-square-o text-vdo-blue'][@title='Vehicle bar graph for last 24 hrs']")
	//static WebElements expandVehichBarGraph;
	@FindBy (xpath = ".//*[@id='p-s-f-vehicle-capacity-wrapper']/div[2]/ion-range-slider/span/span[1]/span[4]")
	static WebElement vCapacityLeftRange;
	@FindBy (xpath = "//i[@class='glyphs text-vdo-blue cursr-pntr']")
	static WebElement printBttn;
	@FindBy (css =".font-xlarge.close-search")
	static WebElement searchCloseIcon;
	@FindBy (css = ".text-uppercase.list-inline-item.activateable.section-vehicles.section-inactive")
	static WebElement clkOnVehicle;
	
	PlannerPage plannerPage= new PlannerPage(driver);
	public CurrentStatus_PlannerPage()
	{
		super();
	
	}
	
	public CurrentStatus_PlannerPage  searchClick()
	{
		wait.forElementVisible(searchIconClick);
		searchIconClick.click();
		test.log(Status.INFO, "Search Icon Clicked");
		return this;
		
	}
	
	public CurrentStatus_PlannerPage verifySliderButton() throws IOException
	{
		
		wait.forElementVisible(driverElement);
		System.out.println(driverElement.getText());
		Assert.assertEquals("DRIVERS", driverElement.getText());
		test.log(Status.PASS, "Asset button Clicked and Driver details displayed" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "asset")));
		return this;
	}
	public CurrentStatus_PlannerPage clickOnFullViewList() throws IOException
	{
		wait.forElementVisible(fullViewListBttn);
		fullViewListBttn.click();
		test.log(Status.INFO, "Full View Button Clicked       :          "+test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "fullview")));
		return this;
	}
	
	public CurrentStatus_PlannerPage verifyfullViewMode() throws IOException
	{
		wait.forElementVisible(mapViewlinnk);
		System.out.println(mapViewlinnk.getText());
		Assert.assertTrue(mapViewlinnk.getText().contains("Map View"));
		test.log(Status.PASS, "Full View Mode Displayed" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "fullviewmode")));
		return this;
	}
	public CurrentStatus_PlannerPage clickOnOnlinetoggle()
	{
		wait.forElementVisible(toggleBttn);
		toggleBttn.click();
		test.log(Status.INFO, "Slider clicked to on");
		return this;
	}
	public CurrentStatus_PlannerPage clickOnMapView() throws InterruptedException
	{
		wait.forElementVisible(mapViewlinnk);
		mapViewlinnk.click();
		Thread.sleep(5000);
		return this;
		
	}
	public CurrentStatus_PlannerPage verifyMapView()
	{
		wait.forElementVisible(zoomInBttn);
		Assert.assertTrue(zoomInBttn.isDisplayed());
		test.log(Status.INFO, "Map view Dispalyed");
		return this;
	}
	
	/** This Method will  return the total number of Drivers available  in Asset
	 * @throws InterruptedException */
	public static int getTotalDriversInAsset() throws InterruptedException
	{
		
		
		Thread.sleep(3000);
		int driversCount =0;
		String driverName=null;
		if(totalDrivers.isEmpty())
		{

			return driversCount;
		}
		else
		{
			List<WebElement>drivers=driver.findElements(By.xpath("//span[@class='p-s-list-fb-200px driverName txt-overflow']"));
			
			for(WebElement ele:drivers)
			{
				driverName=ele.getText();
				driversCount = driversCount +1;
				System.out.println(driverName);
			}
			System.out.println("Total Drivers:  "+driversCount);
			return driversCount;
		}
		

		
		//wait.forElementVisible(totalDrivers);
		
	}
	/**
	 * Get the total Driver Element in Asset
	 * */
	public static List<WebElement> getAvailableDriversInAsset()
	{
		List<WebElement>driverElement = new ArrayList<WebElement>();
		List<WebElement>drivers=driver.findElements(By.xpath("//span[@class='p-s-list-fb-200px driverName txt-overflow']"));
		
		if(drivers.isEmpty())
		{
			driverElement= null;
			test.log(Status.INFO, "No Drivers Availalbe to search");
		}
		else{
		for (WebElement ele:drivers)
		{
			driverElement.add(ele);
		}
		
		}
		return driverElement;
		
	}
	/**
	 * Return a driver from Asset to use in Search
	 * */
	public static String getDriverName()
	{
		
		String driverfromAsset = null;
		if(CurrentStatus_PlannerPage.getAvailableDriversInAsset().isEmpty())
		{
			driverfromAsset = null;
		}
		else
		{
		
		driverfromAsset =CurrentStatus_PlannerPage.getAvailableDriversInAsset().get(0).getText();
		}
		
		return driverfromAsset;
		
	}
	/**Verify the search result in Asset
	 * @throws IOException 
	 * @throws InterruptedException */
	public static void veriFyTheSearch(String dName) throws IOException, InterruptedException
	{
		//String driverfromAsset =CurrentStatus_PlannerPage.getAvailableDriversInAsset().get(0).getText();
		Thread.sleep(5000);
		System.out.println(dName);
		System.out.println(CurrentStatus_PlannerPage.getAvailableDriversInAsset().get(0).getText());
		if(dName.contains(CurrentStatus_PlannerPage.getAvailableDriversInAsset().get(0).getText()))
		{
			test.log(Status.INFO, "Driver Searched" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "SearchResultScreen"+dName)));
		}
		else
		{
			Assert.assertTrue(false, "Search result should display");
		}
	}
	/**
	 * CurrentStatus_PlannerPage clickOnVideoCounterIcon(int totalDrvCount):
	 * THis Method is used to click on video counter data icon in the asset
	 * @param totalDrvCount
	 * @return
	 * @throws IOException
	 */
	
	public CurrentStatus_PlannerPage clickOnVideoCounterIcon(int totalDrvCount) throws IOException
	{
		if(totalDrvCount >0)
		{

			wait.forElementVisible(drivinghrsIcon);
			drivinghrsIcon.click();
			test.log(Status.INFO, "driving hour element clicked");
		}
		else
		{
			test.log(Status.INFO, "No Driver Data Available for the current Selection" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "drvData")));
		}
		return this;
		
	}
	/**
	 * 
	 * @param totalDrvCount
	 * @return
	 * @throws IOException
	 */
	public CurrentStatus_PlannerPage verifyVideoCounterInfoPage(int totalDrvCount) throws IOException
	{
		if(totalDrvCount >0)
		{
			wait.forElementVisible(closeBttnDrivinghrs);
			closeBttnDrivinghrs.click();
			test.log(Status.INFO, "Driving Hours Page displayed");
			Assert.assertTrue(true);
		}
		else
		{
			test.log(Status.INFO, "No Driver Data Available for the current Selection" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "drvData")));
		}
		return this;
	}
	
	/**
	 *Moving slider to extreme left
	 */
	public void clickOnslider() throws InterruptedException
	{

			
		if(CurrentStatus_PlannerPage.findLeftRangeRemainingDrivingTime()==3 )
		{


		
			int x=10;
			Thread.sleep(2000);

			//WebElement slider = driver.findElement(By.xpath(".//*[@id='p-s-f-remaining-driving-time-wrapper']/div[2]/ion-range-slider/span/span[6]"));
			WebElement slider = driver.findElement(By.xpath(".//*[@id='p-s-f-remaining-driving-time-wrapper']/div[2]/ion-range-slider/span/span[3]"));
			CommonExpectedConditions.highLightElement(driver, slider);   

			int width=slider.getSize().getWidth();
			Actions move = new Actions(driver);
			move.moveToElement(slider, ((width*x)/100), 0).click();

			move.build().perform();
			Thread.sleep(3000);
			test.log(Status.INFO, "Remaining Driving Time slider moved to left");
			System.out.println("Slider moved");
		}
	}

	/**
	 * This method is used to move the remaining driving time slider point  left to "0"
	 * @throws InterruptedException
	 */
	public void clickOnslider1() throws InterruptedException
	{
		
		if(CurrentStatus_PlannerPage.findRightRangeRemainingDrivingTime()==8)
		{

			int x=5;
			Thread.sleep(5000);
			//	 WebElement element = driver.findElement(By.xpath(".//*[@id='btn-opener']/img"));


			WebElement slider = driver.findElement(By.xpath(".//*[@id='p-s-f-remaining-driving-time-wrapper']/div[2]/ion-range-slider/span/span[7]"));
			CommonExpectedConditions.highLightElement(driver, slider);   

			//  int width=slider.getSize().getWidth();
			Actions move = new Actions(driver);
			/*  Moving Slider to one point */
			// move.dragAndDropBy(slider, 15, 0);

			/*Moving Slider to last*/
			move.dragAndDropBy(slider, 40, 0);
			// move.clickAndHold(slider);
			//   move.moveByOffset(40, 0);
			move.build().perform();
			Thread.sleep(2000);
			test.log(Status.INFO, "Remaining Driving Time slider moved to right");
			System.out.println("Slider moved");
		}
		
	}
	
	
	/**
	 * This method is used to calculate the remaining driving time in the Assets for Current Status Screen
	 * @return
	 * @throws InterruptedException
	 * @throws IOException
	 */
	
	public CurrentStatus_PlannerPage verifyRemainingDrivingTime() throws InterruptedException, IOException
	{
		List<WebElement>drivers=driver.findElements(By.xpath("//span[@class='p-s-list-fb-200px driverName txt-overflow']"));
		
		if(drivers.isEmpty())
		{
			test.log(Status.INFO, "No drivers available for the current selection in the assests" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "SearchResultScreen")));
		}
		else
		{
			for(WebElement ele1:drivers)
			{
				String driverName=ele1.getText();

				Thread.sleep(10000);
				List<WebElement>remainingTime=driver.findElements(By.xpath("//div[@class='flexbox flex-center mr-1 pl-21px']/span[@class='text-gray-normal']"));
				for(WebElement ele:remainingTime)
				{

					String[]  tTime = ele.getText().split(":");
					int rTime=Integer.parseInt(tTime[0]);

					if (rTime>=0&rTime<=10)
					{
						Assert.assertTrue(true,"Currect Drivers are available in slider");

					}
					else
					{
						test.log(Status.FAIL, "available driver is "+driverName);
						Assert.assertTrue(false,"Wrong Drivers in Slider");
					}

				}
			}
		}
		return this;
		
	}
	public static int findLeftRangeRemainingDrivingTime()
	{
		wait.forElementVisible(leftRangeRemainigDrivingTime);
		int range1=Integer.parseInt(leftRangeRemainigDrivingTime.getText());
		System.out.println("Range"+range1);
		return range1;
		
	}
	public static int findRightRangeRemainingDrivingTime()
	{
		wait.forElementVisible(rightRangeRemainigDrivingTime);
		int range1=Integer.parseInt(rightRangeRemainigDrivingTime.getText());
		System.out.println("Range"+range1);
		return range1;
	}

	public void defaultValueForRemainingDrivingTime()
	{
		if(CurrentStatus_PlannerPage.findRightRangeRemainingDrivingTime()==8 & CurrentStatus_PlannerPage.findLeftRangeRemainingDrivingTime()==3)
		{
			Assert.assertTrue(true, "Default Value Matched");
			test.log(Status.PASS, "Default value displayed");
			
		}
		else
		{
			Assert.assertTrue(false, "default value not matched");
			
		}
		
	}
	
	/**
	 * 
	 * @param sites
	 * @throws InterruptedException
	 */

	public void verifyUpdatedSiteInCurrentStatusPage(String sites) throws InterruptedException
	{
		Thread.sleep(3000);
		String site= driver.findElement(By.xpath(".//*[@id='dropdownMenuButton1']")).getText();
		Assert.assertEquals(site, sites);
		test.log(Status.INFO, "The site matched with the setting");
	}
	public static void clickOnSorting() throws InterruptedException
	{
		Thread.sleep(2000);
		wait.forElementVisible(clickOnSorting);
		clickOnSorting.click();
	}
	public static void sortingByActivityType() throws InterruptedException
	{
		Thread.sleep(1000);
		List<WebElement> ele = driver.findElements(By.xpath("//a[@class='dropdown-item']"));
	    for (WebElement sortingItem:ele)
	    {
	    	String sortType = sortingItem.getText();
	    	if(sortType == "ActivityType")
	    	{
	    		sortingItem.click();
	    	}
	    }
	}
	public static void verifySorting()
	{
		wait.forElementVisible(firstelementDriving);
		Assert.assertTrue(firstelementDriving.isDisplayed());
		test.log(Status.INFO, "Drivers sorted by activity type");
		
	}
	public static void chooseLocations()
	{
		wait.forElementVisible(chooseLocation);
		chooseLocation.sendKeys("London");
		chooseLocation.sendKeys(Keys.ENTER);
	}
	
	public static void verifyLocationOnMap() throws IOException
	{
		test.log(Status.INFO, "pass: " + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "verifyLocationOnMap")));
	}
	public static void verifyExpandForVehicleGraph() throws InterruptedException, IOException
	{
		//wait.forElementVisible(expandVehichBarGraph);
		List<WebElement>eleBarGraphIcon=driver.findElements(By.xpath(".//*[@id='p-s-driver-list']/div/div/div[3]/i"));
		if (eleBarGraphIcon.isEmpty())
		{
			test.log(Status.INFO, "No Vehicles Available in the current Selection");
		}
		else
		{
			for (WebElement barGraphIcon:eleBarGraphIcon)
			{
				barGraphIcon.click();
				String getMinus =barGraphIcon.getAttribute("class");
				if (getMinus.contains("fa text-vdo-blue fa-minus-square"))
				{
					Assert.assertTrue(true);
					test.log(Status.INFO, "Vehicle Bar Graph Displayed" +test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "SearchResultScreen")));
				}
				else
				{
					test.log(Status.INFO, "Vehicle Bar Graph not Displayed");
					Assert.assertTrue(false);
				}
				
				//System.out.println(getMinus);
				Thread.sleep(2000);
			}
		}
		
		
	}
	public static void verifyVRNExists() throws InterruptedException
	{
		
		List<WebElement>eleBarGraphIcon=driver.findElements(By.xpath("//span[@class='mr-1 mr-100px']"));
		if (eleBarGraphIcon.isEmpty())
		{
			test.log(Status.INFO, "No VRN Available in the current Selection");
		}
		else
		{
			for (WebElement barGraphIcon:eleBarGraphIcon)
			{
				String getMinus =barGraphIcon.getText();
				if (getMinus.isEmpty())
				{
					Assert.assertTrue(false);
					test.log(Status.INFO, "VRN not displayed");
				}
				else
				{
					test.log(Status.INFO, "VRN : " +getMinus+"  displayed" );
					//Assert.assertTrue(true);
				}
				
				System.out.println("VRN : " +getMinus+"  displayed");
				Thread.sleep(2000);
			}
		}
	}

	public static String getVehicleName()
	{
		if (CurrentStatus_PlannerPage.getAvailableVehiclesInAsset().isEmpty())
		{
			test.log(Status.INFO, "No Vehicle(s) Available for current selection");
		}
		{
		
			String vehiclefromAsset =CurrentStatus_PlannerPage.getAvailableVehiclesInAsset().get(0).getText();
		
			return vehiclefromAsset;
		}
	}
	public static List<WebElement> getAvailableVehiclesInAsset()
	{
		List<WebElement>vehicleElement = new ArrayList<WebElement>();
		List<WebElement>vehicles=driver.findElements(By.xpath("//div[@class='fb-full-1340px flexbox p-s-list-fb-200px']/span"));
		for (WebElement ele:vehicles)
		{
			vehicleElement.add(ele);
		}
		return vehicleElement;
		
	}
	public static String getLeftRangeOfVehicleCapacitySlider()
	{
		wait.forElementVisible(vCapacityLeftRange);
		String vCapacity = vCapacityLeftRange.getText();
		System.out.println(vCapacity);
		return vCapacity;
		
	}
	
	
	/**
	 * Method to move the Vehicle Capacity Slider
	 * @throws InterruptedException
	 * @throws IOException
	 */
	
	public static void movingVehicleCapacityslider() throws InterruptedException, IOException
	{
		if (CurrentStatus_PlannerPage.getLeftRangeOfVehicleCapacitySlider().contains("50%")) 
		{

			int x=10;
			Thread.sleep(4000);

			//WebElement slider = driver.findElement(By.xpath(".//*[@id='p-s-f-vehicle-capacity-wrapper']/div[2]/ion-range-slider/span/span[3]"));
			WebElement slider = driver.findElement(By.cssSelector("#p-s-f-vehicle-capacity-wrapper>div>ion-range-slider>span>span.irs-bar"));
			CommonExpectedConditions.highLightElement(driver, slider);   

			int width=slider.getSize().getWidth();
			Actions move = new Actions(driver);
			move.moveToElement(slider, ((width*x)/100), 0).click();

			move.build().perform();
			Thread.sleep(4000);
			String vehicleCapacity = driver.findElement(By.xpath(".//*[@id='p-s-f-vehicle-capacity-wrapper']/div[2]/ion-range-slider/span/span[1]/span[4]")).getText();
			if (vehicleCapacity.contentEquals("0%")) 
			{
				test.log(Status.INFO, "VehicleCapacity slider moved to left"+test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Vehicle Slider")));
			}
			else
			{
				Assert.assertTrue(false);
			}

			System.out.println("Slider moved");
		}

	
	}
	public static void moveVehicleSliderTo80() throws InterruptedException, IOException
	{
		int x=5;
		 Thread.sleep(5000);

		 //WebElement slider = driver.findElement(By.xpath(".//*[@id='p-s-f-vehicle-capacity-wrapper']/div[2]/ion-range-slider/span/span[3]"));
		 WebElement slider = driver.findElement(By.cssSelector("span[class='irs js-irs-1']>span[class='irs-slider from type_last']"));
		// WebElement draggablePartOfScrollbar = driver.findElement(By.cssSelector("span[class='irs-slider to']"));
		 CommonExpectedConditions.highLightElement(driver, slider);   
		 	
		   // int width=slider.getSize().getWidth();
		  //  Actions move = new Actions(driver);
		  //  move.moveToElement(slider, ((width*x)/100), 0).click();
		  
		  //   move.build().perform();
		 //    Thread.sleep(10000);
		     
		     Actions move = new Actions(driver);
		   //  WebElement draggablePartOfScrollbar = driver.findElement(By.xpath("**xpath of slider**"));
		    // int numberOfPixelsToDragTheScrollbarDown = -1000;
		    // dragger.moveToElement(draggablePartOfScrollbar).clickAndHold().moveByOffset(0,numberOfPixelsToDragTheScrollbarDown).release().perform();
		     
		 
		  //  int width=slider.getSize().getWidth();
		  // Actions move = new Actions(driver);
		   /*  Moving Slider to one point */
		     move.dragAndDropBy(slider, 70, 0);
		    // move.sendKeys(Keys.ARROW_LEFT);
		
		    /*Moving Slider to last*/
		   // move.dragAndDropBy(slider, 40, 0);
		   // move.clickAndHold(slider);
		   // move.moveByOffset(15, 0);
		    move.build().perform();
		     Thread.sleep(5000);
		    String vehicleCapacity = driver.findElement(By.xpath(".//*[@id='p-s-f-vehicle-capacity-wrapper']/div[2]/ion-range-slider/span/span[1]/span[4]")).getText();
		    if (vehicleCapacity.contentEquals("80%")) 
		    {
		    	test.log(Status.INFO, "VehicleCapacity slider moved to left"+test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Vehicle Slider")));
		    }
		    else
		    {
		    	Assert.assertTrue(false);
		    }
		     
		     System.out.println("Slider moved");
	}

	public static void verifyTheVehicleCapacityOfTheAvailableVehilces()
	{
		
		
		
	}

	/**
	 * This method is used to click on the print icon available in the currentStatus Screen.
	 */
	public static void clickOnPrint()
	{
		wait.forElementClickable(printBttn);
		printBttn.click();
		
	}
   
	public static void closePrintPupUp() throws InterruptedException, AWTException
	{
		
	
		Robot robot = new Robot();
		Thread.sleep(8000);


		robot.keyPress(KeyEvent.VK_ESCAPE);
		
		robot.keyRelease(KeyEvent.VK_ESCAPE);
	
		driver.switchTo().window(driver.getWindowHandles().toArray()[0].toString());
		
		
		JavascriptExecutor executor = (JavascriptExecutor) driver.getWindowHandles();
		executor.executeScript("document.getElementsByClassName('cancel')[1].click();");
		Thread.sleep(6000);
		System.out.println("PrintCanceled");
		
	}

	public static void closeSearchTxtBox()
	{
		wait.forElementClickable(searchCloseIcon);
		searchCloseIcon.click();
		
	}

	public static void clickOnVehicleTab() {
		wait.forElementClickable(clkOnVehicle);
		clkOnVehicle.click();
		
	}
	

}

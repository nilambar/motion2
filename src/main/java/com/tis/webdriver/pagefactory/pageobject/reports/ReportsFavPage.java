package com.tis.webdriver.pagefactory.pageobject.reports;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.html5.AddApplicationCache;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.model.Log;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;

public class ReportsFavPage extends TisBasePageObject
{
	@FindBy (xpath ="//div[@class='row favourite-reports reports-section']/div/div/div/div/i")
	static List<WebElement> favElements;
	@FindBy (css = "div[class='card-content']")
	static WebElement reports;
	@FindBy (xpath  ="//h2[contains(text(),'Driver Shift Protocol')]")
	static List<WebElement> driverShiftProtocol;
	@FindBy (xpath = "//div[@id='my-reports']/div/div/div/div/i")
	static List<WebElement> favElementsInReports;
	@FindBy (xpath=".//*[@id='btn-drivers-legal']/span")
	static WebElement vehiclesTabInReport;
	
	
	public static void checkIfFavReportsExist() throws InterruptedException
	{
		Thread.sleep(3000);
		if (favElements.size()>0)
		{
			System.out.println(favElements.size());
			test.log(Status.INFO, "Favourite Reports Existing");
			for(WebElement ele:favElements)
			{
				Thread.sleep(1000);
				ele.click();
				test.log(Status.INFO, "Favourite Reports Removed");
			}
		}
		else
		{
			test.log(Status.INFO, "No existing favourite reports");
		}
		
	}
	
	
	
	public static void clickOnFavIconOfDriverShiftProtocol()
	{
		if(reports.isDisplayed())
		{
			List<WebElement> favs = driver.findElements(By.xpath("//i[@class='glyphs card-star']"));
			
			for(WebElement ele:favs)
			{
				System.out.println(ele.getSize());
			}
			favs.get(0).click();
			test.log(Status.INFO, "Reports clicked");
		}
	}
	
	

	public static void verifyFavouritesAddedInFavouriteSection() throws InterruptedException, IOException
	{
		
		Thread.sleep(3000);
		System.out.println(favElements.size());
		if(favElements.size()>=1)
			
		{
			test.log(Status.PASS, "Favourites  Added in the Favourite Sections" +test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "favReports")));
		}
		else
		{
			Assert.assertTrue(false);
		}

	}
	
	public static void verifyFavouriteOfEventDetailsProtocol()
	{
		if(driverShiftProtocol.size()>1)
		{
			test.log(Status.INFO, "Favourite Added");
		}
		else
		{
			Assert.assertTrue(false);
		}
			
		
	}
	
	public static void clickOnFavIconsInReports() throws InterruptedException
	{
		Thread.sleep(5000);
		if(favElementsInReports.size()>0)
		{
			
			for (WebElement ele:favElementsInReports)
			{
				Thread.sleep(1000);
				ele.click();
			}
		}
	}
	
	public static void clickOnVehicleTab() throws InterruptedException
	{
		Thread.sleep(5000);
		wait.forElementClickable(vehiclesTabInReport);
		vehiclesTabInReport.click();
		
	}
	
}

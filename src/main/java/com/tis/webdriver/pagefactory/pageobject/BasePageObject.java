package com.tis.webdriver.pagefactory.pageobject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;



import org.openqa.selenium.support.ui.WebDriverWait;

import com.tis.webdriver.common.template.DriverProvider;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.common.template.NewTestTemplate;
//import com.wikia.webdriver.common.core.WikiaWebDriver;
import com.tis.webdriver.common.core.element.JavascriptActions;
import com.tis.webdriver.common.core.element.Wait;

public class BasePageObject extends NewTestTemplate
{
	 private static final int TIMEOUT_PAGE_REGISTRATION = 3000;
	  public static Wait wait;
	  public WebDriverWait waitFor;
	  public Actions builder;
	  protected int timeOut = 15;

	  protected com.tis.webdriver.common.core.element.JavascriptActions jsActions;
	 // protected static WebDriver driver=DriverProvider.getDriver("chrome");
	
	public BasePageObject()
	{
		this.waitFor = new WebDriverWait(driver, timeOut);
	    this.builder = new Actions(driver);
	    this.wait = new Wait(driver);
	    this.jsActions = new JavascriptActions(driver);
	  	PageFactory.initElements(driver, this);
		
	}
	
	public static String getTimeStamp() {
	    Date time = new Date();
	    long timeCurrent = time.getTime();
	    return String.valueOf(timeCurrent);
	  }
	
	 /**
	   * Simple method for checking if element is on page or not. Changing the implicitWait value allows
	   * us no need for waiting 30 seconds
	   */
	  protected boolean isElementOnPage(By by) {
	    changeImplicitWait(500, TimeUnit.MILLISECONDS);
	    try {
	      return driver.findElements(by).size() > 0;
	    } finally {
	      restoreDefaultImplicitWait();
	    }
	  }
	  
	  protected void changeImplicitWait(int value, TimeUnit timeUnit) {
		    driver.manage().timeouts().implicitlyWait(value, timeUnit);
		  }

		  protected void restoreDefaultImplicitWait() {
		    changeImplicitWait(timeOut, TimeUnit.SECONDS);
		  }
		  
		  public String switchToNewBrowserTab() {
			    List<String> tabs = new ArrayList<String>(driver.getWindowHandles());
			    driver.switchTo().window(tabs.get(tabs.size() - 1));

			    return driver.getCurrentUrl();
			  }

			  private int getTabsCount() {
			    return driver.getWindowHandles().size();
			  }

			  private String getNewTab(String parentTab) {
			    Optional<String> newTab = driver
			      .getWindowHandles()
			      .stream()
			      .filter(handleName -> !handleName.equals(parentTab))
			      .findFirst();
			    return newTab.orElseThrow(() -> new NotFoundException("New tab not found!"));
			  }

			  private String switchToNewTab(String parentTab) {
			    String newTab = getNewTab(parentTab);
			    driver.switchTo().window(newTab);
			    return newTab;
			  }

			  /**
			   * Simple method for checking if element is on page or not. Changing the implicitWait value allows
			   * us no need for waiting 30 seconds
			   */
			  protected boolean isElementOnPage(WebElement element) {
			    changeImplicitWait(500, TimeUnit.MILLISECONDS);
			    boolean isElementOnPage = true;
			    try {
			      // Get location on WebElement is rising exception when element is not present
			      element.getLocation();
			    } catch (WebDriverException ex) {
			      isElementOnPage = false;
			    } finally {
			      restoreDefaultImplicitWait();
			    }
			    return isElementOnPage;
			  }

			  /**
			   * Method to check if WebElement is displayed on the page
			   *
			   * @return true if element is displayed, otherwise return false
			   */

			  protected boolean isElementDisplayed(WebElement element) {
			    try {
			      return element.isDisplayed();
			    } catch (NoSuchElementException e) {
			      test.error("Element not Found" +e);
			      return false;
			    }
			  }

			  /**
			   * Make sure element is ready to be clicked and click on it The separation of this method has
			   * particular reason. It allows global modification of such click usages. This way it is very easy
			   * to control what criteria have to be met in order to click on element
			   *
			   * @param element to be clicked on
			   */
			  protected void waitAndClick(WebElement element) {
			    wait.forElementClickable(element).click();
			  }

			  /**
			   * Simple method for getting number of element on page. Changing the implicitWait value allows us
			   * no need for waiting 30 seconds
			   */
			  protected int getNumOfElementOnPage(By cssSelectorBy) {
			    changeImplicitWait(500, TimeUnit.MILLISECONDS);
			    int numElementOnPage;
			    try {
			      numElementOnPage = driver.findElements(cssSelectorBy).size();
			    } catch (WebDriverException ex) {
			      numElementOnPage = 0;
			    } finally {
			      restoreDefaultImplicitWait();
			    }
			    return numElementOnPage;
			  }

			  protected boolean isElementInContext(String cssSelector, WebElement element) {
			    changeImplicitWait(500, TimeUnit.MILLISECONDS);
			    boolean isElementInElement = true;
			    try {
			      if (element.findElements(By.cssSelector(cssSelector)).size() < 1) {
			        isElementInElement = false;
			      }
			    } catch (WebDriverException ex) {
			      isElementInElement = false;
			    } finally {
			      restoreDefaultImplicitWait();
			    }
			    return isElementInElement;
			  }

			  protected void scrollTo(WebElement element) {
			    jsActions.scrollElementIntoViewPort(element);
			    wait.forElementClickable(element, 5);
			  }

			  protected void scrollAndClick(WebElement element) {
			    jsActions.scrollElementIntoViewPort(element);
			    wait.forElementClickable(element, 5);
			    element.click();
			  }

			  protected void scrollAndClick(List<WebElement> elements, int index) {
			    jsActions.scrollElementIntoViewPort(elements.get(index));
			    wait.forElementClickable(elements, index, 5);
			    elements.get(index).click();
			  }

			  protected void scrollAndClick(WebElement element, int offset) {
			    jsActions.scrollToElement(element, offset);
			    element.click();
			  }

			  public boolean isStringInURL(String givenString) {
			    String currentURL = driver.getCurrentUrl();
			    if (currentURL.toLowerCase().contains(givenString.toLowerCase())) {
			    	test.info("Url contains string");
			      return true;
			    } else {
			    	test.info("Url doesnot contains string");
			      return false;
			    }
			  }


	protected void getURL(String url)
	{
		getURL(url,true);
	}

	private void getURL(String url, boolean urlPresent)
	{
		driver.get(url);
		/*if(urlPresent)
		{
			Reporter.log("Url present");
		}*/
	}

}

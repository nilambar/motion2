package com.tis.webdriver.pagefactory.pageobject.homepage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;






import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.core.element.Wait;
import com.tis.webdriver.common.utility.SyncUtility;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;
import com.tis.webdriver.pagefactory.pageobject.notifications.NotificationsPage;
import com.tis.webdriver.pagefactory.pageobject.planner.PlannerPage;

public class TisHomePage extends TisBasePageObject
{
	 
	
	//@FindBy (xpath = "//a/span[contains(text(),'Notifications')]")
	@FindBy (xpath = "//*[@id='notifications-tab']/a")
	WebElement notificationLink;
	
	@FindBy (xpath = ".//*[@id='planner-tab']/a")
    WebElement plannerLink;
	@FindBy (xpath="//span[contains(text(),'COMMUNICATION')]")
	WebElement communicationLink;
	@FindBy (xpath = ".//*[@id='reports-tab']")
	static WebElement reportsLink;
	@FindBy (xpath=".//*[@id='dashboard-tab']/a")
	static WebElement dashboardLink;

	
	public TisHomePage(WebDriver driver)
	{
		super();
		PageFactory.initElements(driver, this);
			
	}
	Wait wait = new Wait(driver);

	public  void clickNotification() throws InterruptedException
	{
		
	
		SyncUtility.isElementPresnt(driver, ".//*[@id='notifications-tab']/a/span", 20);
		Thread.sleep(4000);
		
		//wait.forElementVisible(notificationLink);
		driver.findElement(By.xpath(".//*[@id='notifications-tab']/a/span")).click();
		
	//	notificationLink.click();
		test.log(Status.INFO, "Notification link clicked");
		//return new NotificationsPage(driver);
		
	}

	
	public TisHomePage clickNotificationIcon()
	{
		
		return this;
	}

	/**Click on Planner Link in the home page*/
	public  PlannerPage clickOnPlanner()
	{
		
		wait.forElementClickable(plannerLink);
		plannerLink.click();
		test.log(Status.INFO, "Planner link clicked");
		return new PlannerPage(driver);
		
	}

	public void clickCommunication() {
		
		wait.forElementVisible(communicationLink);
		communicationLink.click();
		// TODO Auto-generated method stub
		
	}

	public void clickOnReports() throws InterruptedException 
	{
		wait.forElementClickable(reportsLink, 20);
		reportsLink.click();
		test.log(Status.INFO, "Reports Link Clicked");
	}


	public void clickOnDashBoard()
	{
		wait.forElementClickable(dashboardLink);
		dashboardLink.click();
		test.log(Status.INFO, "Dashboard link clicked");
	}

}

package com.tis.webdriver.pagefactory.pageobject.planner;



import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.core.element.CommonExpectedConditions;
import com.tis.webdriver.common.core.element.Wait;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;

public class Geofence_PlannerPage extends TisBasePageObject
{
	@FindBy (xpath = "//div[@class = 'slider']")
	static 	WebElement togglwShowGeofence;
	@FindBy (xpath=".//*[@id='section-title']")
	static WebElement geofenceTitle;
	@FindBy(xpath = "//span[text()='GEOFENCING']")
	static WebElement geofenceTab;
	@FindBy (css=".font-xlarge.close-search")
	static WebElement closeBttn;
	@FindBy(css = ".glyphs.mr-2.create-geofence")
	static WebElement openGeoFencePage;
	
	boolean status;
	static Wait wait = new Wait(driver);
	
	public Geofence_PlannerPage(WebDriver driver)
	{
		super();
	}
	
	public static void verifyPageHeaderGeofence()
	{
		
		wait.forElementVisible(geofenceTitle);
		if (geofenceTitle.getText().contains("Geofence"))
		{
			Assert.assertTrue(true, "The page header displayed as: Geofence "); 
			test.log(Status.PASS, "The page header displayed as: Geofence");
		}
		else
		{
			Assert.assertTrue(false,"Geofence Page Not Displayed");
		}
		
	}
	public void clickShowGeofence() throws InterruptedException, IOException
	{
		Thread.sleep(2000);
	//	driver.findElement(By.xpath("//div[@class = 'slider']")).click();
		
		CommonExpectedConditions.highLightElement(driver, togglwShowGeofence);
		Thread.sleep(3000);
		if (togglwShowGeofence.isDisplayed())
		{
			togglwShowGeofence.click();
			test.log(Status.INFO, "User Clicked on Show Geofence Toggle");
			Thread.sleep(1000);
			
			test.log(Status.PASS, "Pass: " + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Map")));
			Assert.assertTrue(true);
		}
		else
		{
			Assert.assertTrue(false);
			test.log(Status.ERROR, "GeoFence toggle bar not displayed");
		
		}
		
	}
	
	public void verifyGeoFenceInTheMap() throws FindFailed
	{
		Screen screen = new Screen();
		Pattern switchtoTis = new Pattern("C:\\WorkSapace\\ScreenShorts\\Capture.PNG");
		if (screen.exists(switchtoTis, 20)!=null)
		{System.out.println("picture found");
		screen.click(switchtoTis);
		 status = true;
		}
		else
		{
				status=false;
		}
	
	}
	
	
	public void clickOnGeoFence()
	{
		wait.forElementClickable(geofenceTab);
		geofenceTab.click();
		
	}
	public static void closeSearchBox()
	{
		wait.forElementClickable(closeBttn);
		closeBttn.click();
	}

	public void openCreateGeofenceIcon()
	{
		wait.forElementClickable(openGeoFencePage);
		openGeoFencePage.clear();
		
		
		
	}
		
}



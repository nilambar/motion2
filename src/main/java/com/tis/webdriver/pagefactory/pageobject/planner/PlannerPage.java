package com.tis.webdriver.pagefactory.pageobject.planner;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.core.element.CommonExpectedConditions;
import com.tis.webdriver.common.core.element.Wait;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;

public class PlannerPage extends TisBasePageObject
{
	@FindBy (xpath = ".//*[@id='section-title']")
	static 	WebElement plannerTitle;
	@FindBy (xpath = ".//*[@id='btn-planner-assets']/a/span")
	static WebElement geofenceTab;
	@FindBy (xpath = ".//*[@id='section-title']")
	static WebElement geofenceTitle;
	@FindBy (xpath = ".//*[@id='page-wrapper']/app-current-status/planner-common-menu/div/a/i")
	static WebElement settingIcon;
	@FindBy (xpath = ".//*[@id='dropdownMenuButton1']")
	static WebElement allSitesDropMenu;
	@FindBy (xpath = ".//*[@id='dropdownMenuButton2']")
	static WebElement allDrivers;
	@FindBy (xpath = ".//*[@id='dropdownMenuButton3']")
	static WebElement allVehicles;
	@FindBy (xpath="//div[@id='btn-planner-site']")
	static WebElement traceBttn;
	@FindBy(xpath="//div[@id='btn-opener']/img")
	static WebElement openAssetBttn;
	@FindBy(css="img[alt='sliderbutton']")
	static WebElement closeAssetBttn;
	@FindBy(xpath="//input[@id='autorfreshCheck'][@type ='checkbox']")
	static WebElement autoRefresh;
	@FindBy(css=".glyphs.p-0.blue-dark.font-weight-bold.mr-20px.font-xlarge")
	static WebElement searchIcon;
	@FindBy(css="input[placeholder='Search']")
	static WebElement searchTextBox;
	@FindBy(css=".font-xlarge.close-search")
	static WebElement closeSearchIcon;
	@FindBy(css=".update-time-format.updatetime")
	static WebElement retrieveTheDate;
	@FindBy (css=".glyphs.font-weight-bold.blue-dark.nopadding.mt-20px.autorefresh")
	static WebElement clickOnRefresh;
	static By totalDriversInList=By.cssSelector("div[aria-labelledby='dropdownMenuButton2']>a");
	static boolean isChecked;
	static Wait wait = new Wait(driver);
			
	public PlannerPage(WebDriver driver)
	{
		super();
		PageFactory.initElements(driver, this);
	}
	public static Geofence_PlannerPage openGeofenceTab() throws InterruptedException 
	{
	
		wait.forElementVisible(geofenceTab);
		geofenceTab.click();
		return new Geofence_PlannerPage(driver);
		
	}
	public Trace_PlannerPage openTraceTab()
	{
		wait.forElementVisible(traceBttn);
		traceBttn.click();
		return new Trace_PlannerPage(driver);
		
	}
	public static Settings_PlannerPage openPlannerSettings() throws InterruptedException
	{
		WebDriverWait sleep = new WebDriverWait(driver, 30);
		
		sleep.until(ExpectedConditions.elementToBeClickable(settingIcon));
		//wait.forElementVisible(settingIcon);
		
		Thread.sleep(10000);
		settingIcon.click();
		return new Settings_PlannerPage(driver);
	}
	public static void verifyPageHeaderPlanner()
	{
		
		Assert.assertEquals(plannerTitle.getText(), "Planner");
				
	}
	public static List<WebElement> getAvailableSitesInDropMenu() throws InterruptedException
	{
		wait.forElementClickable(allSitesDropMenu);
		Thread.sleep(10000);
		allSitesDropMenu.click();
		
		ArrayList<String> totalElements=new ArrayList<String>();
		
		List<WebElement> siteElements=new ArrayList<WebElement>();
		Thread.sleep(2000);
		siteElements = driver.findElements(By.xpath(".//*[@id='PlannerMenuCurrentStatus']/div/div[1]/div[1]/div/a"));
		
		//List<WebElement> totalElements = driver.findElements(By.cssSelector(".dropdown-menu.top-blue-list"));
		System.out.println(totalElements);
		for (WebElement ele:siteElements)
		{
			String site = ele.getText();
			totalElements.add(site);
			
			System.out.println( site);
			test.log(Status.PASS, site)	;
			//ele.click();
		}
		Assert.assertTrue(true);
		
			//System.out.println("Total driver verified");
		return 	siteElements;
	}
	/**
	 * This Method will return all  Vehicles element available in list
	 * */
	public static List<WebElement>  verifyAvailableVehiclesInDropMenu() throws InterruptedException
	{
		ArrayList<String> vehicleList = new ArrayList<String>();
		List<WebElement> totalElements=new ArrayList<WebElement>();
		int tVehiclesInDropDown = 1;
		Thread.sleep(3000);
		if (allVehicles.isDisplayed())
		{
			allVehicles.click();
			Thread.sleep(2000);
			totalElements  = driver.findElements(By.cssSelector(".dropdown-menu.top-blue-list[aria-labelledby='dropdownMenuButton3']>a"));
			for (WebElement ele:totalElements)
			{
				String tVehicle = ele.getText();
				vehicleList.add(tVehicle);
				test.log(Status.PASS, tVehicle)	;
				System.out.println(tVehicle);
				tVehiclesInDropDown = tVehiclesInDropDown+1;
			}
			Assert.assertTrue(true);
			if(totalElements.size()==1)
			{
				test.log(Status.INFO, "No Vehicles available for the current selection");
			}

		}
		return totalElements;
		
	}
	
	/*public static void verifyAvailableVehiclesInDropMenu() throws InterruptedException
	{
		Thread.sleep(3000);
		if (allVehicles.isDisplayed())
		{
			allVehicles.click();
			Thread.sleep(2000);
			List<WebElement> totalElements = driver.findElements(By.xpath("//div [@class ='dropdown-menu top-blue-list'][@aria-labelledby='dropdownMenuButton2']"));
			for (WebElement ele:totalElements)
			{
				String site = ele.getText();
				//test.log(Status.PASS, site)	;
				System.out.println(site);
			}
			Assert.assertTrue(true);

		}
		else
		{
			Assert.assertTrue(false);
		}
	}*/
	
	/**
	 * This Method will return all  driver elements available in list
	 * */
	public static List<WebElement> totalDriversInDropDown() throws InterruptedException
	{
		wait.forElementVisible(allDrivers);
		allDrivers.click();
		Thread.sleep(2000);
		List<WebElement> totalElements=new ArrayList<WebElement>();
		Thread.sleep(2000);
		totalElements=driver.findElements(totalDriversInList);
		Thread.sleep(2000);
		System.out.println(totalElements);
		int nCount=0;
		Thread.sleep(2000);
		ArrayList<String> driverList = new ArrayList<String>();
		for (WebElement ele:totalElements)
		{
			String tDrivers = ele.getText();
			driverList.add(tDrivers);
			
			System.out.println("The driver name is  :" +tDrivers);
			test.log(Status.PASS, tDrivers)	;
			Assert.assertTrue(true);
		}
		if(nCount==1)
		{
			test.log(Status.INFO, "No Drivers available for the current selection");
		}
		nCount=driverList.size();
		return(totalElements);
	}
	
	/*public static int totalDrivers1InDropDown() throws InterruptedException
	{
		int driversInDropDown = 1;
		
		wait.forElementVisible(allDrivers);

			allDrivers.click();

			List<WebElement> totalElements = driver.findElements(By.xpath("//div[@aria-labelledby='dropdownMenuButton2']/a"));
			System.out.println(totalElements);
			for (WebElement ele:totalElements)
			{
				String tDrivers = ele.getText();
				driversInDropDown=driversInDropDown+1;
				System.out.println("The driver name is  :" +tDrivers);
				test.log(Status.PASS, tDrivers)	;
				Assert.assertTrue(true);
			}
			System.out.println("Total Drivers in DropDown"+driversInDropDown);
			return driversInDropDown;
	}*/
	public CurrentStatus_PlannerPage openDriverVehicleAsset(By sliderBttn2) throws InterruptedException
	{
		Thread.sleep(5000);
		wait.forElementVisible(sliderBttn2);
		//wait.forElementVisible(sliderBttn);
		
		driver.findElement(sliderBttn2).click();
		test.log(Status.INFO, "Slider button Clicked");
		return new CurrentStatus_PlannerPage();
	}
	
	public static void  verifyAvailableDrivers() throws InterruptedException
	{
		Assert.assertEquals(CurrentStatus_PlannerPage.getTotalDriversInAsset(),PlannerPage.totalDriversInDropDown());
		
	}
	public boolean clickOnAutoRefreshCheckBox() throws InterruptedException
	{
		
		
		Thread.sleep(10000);
		wait.forElementVisible(autoRefresh);
		isChecked= autoRefresh.findElement(By.tagName("input")).isSelected();
		System.out.println(isChecked);
		autoRefresh.click();
		Thread.sleep(5000);
		isChecked= autoRefresh.findElement(By.tagName("input")).isSelected();
		System.out.println(isChecked);
		return isChecked;
	}
	public void verifyAutoRefreshCheckBox() 
	{
		if (isChecked == false)
		{
			Assert.assertTrue(true);
			test.log(Status.INFO, "AutoRefresh Check box unchecked");
		}
		else
		{
			Assert.assertTrue(false);
		}
	}
	
	
	public static void clickOnSearch()
	{
		wait.forElementClickable(searchIcon);
		searchIcon.click();
	}
	/**This Method will verify the Search functionality of search Suggestion in the text box*/
	public  void searchSuggestions(String sChar) throws InterruptedException, IOException
	{
		wait.forElementVisible(searchTextBox);
	//	searchTextBox.clear();
		
		//Actions action = new Actions(driver);
		//Actions series = action.moveToElement(searchTextBox).click().sendKeys(searchTextBox, sChar);
		//series.perform();
		searchTextBox.sendKeys(sChar);
		Thread.sleep(6000);
		List<WebElement>driverName=driver.findElements(By.cssSelector(".completer-dropdown"));
		if(driverName.isEmpty())
		{
			Assert.assertTrue(false, "Search result should display");
		}
		else
		{
			System.out.println(driverName);
			for(WebElement ele:driverName)
			{
				System.out.println(ele.getText());
				if(ele.getText().contains("No results found"))
				{
					test.log(Status.INFO, "No results found for current selection"+test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "sChar"+sChar)));
				}
				else if(ele.getText()!=null)
				{
					test.log(Status.INFO, "Search Result displayed"+test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "sChar"+sChar)));
				}
				else if(ele.getText().isEmpty())
				{
					Assert.assertTrue(false, "Search result should display");
				}
				else
				{
				Assert.assertTrue(false, "Search result should display");
				}
			}
		
		}
	
		Thread.sleep(5000);
				
	}
	
	public PlannerPage searchDriverVehicleOnMap(String driverVeh) throws InterruptedException
	{
		Thread.sleep(5000);
		wait.forElementVisible(searchTextBox);
		searchTextBox.clear();
		searchTextBox.sendKeys(driverVeh);
		Thread.sleep(3000);
		List<WebElement>driverName=driver.findElements(By.cssSelector(".completer-dropdown"));
		for(WebElement ele:driverName)
		{
			System.out.println(ele.getText());
			ele.click();
			
		}
		Thread.sleep(5000);
		test.log(Status.INFO, "Enter Key Pressed");
		return this;
	}
	public static void clickOnOpenAsset()
	{
		wait.forElementClickable(openAssetBttn);
		openAssetBttn.click();
		test.log(Status.INFO, "Button clicked to open the assets");
	}
	public static void clickOnAssetCloseImg()
	{
		wait.forElementClickable(closeAssetBttn);
		closeAssetBttn.click();
		test.log(Status.INFO, "Button clicked to open the assets");
	}
	
	
	/**
	 * This method will select a site from the List and take the screenshot of the selected site in the Map*/
	public static void verifySiteInTheMap() throws InterruptedException, IOException
	{
		int i=1;
		List<WebElement> ele= PlannerPage.getAvailableSitesInDropMenu();
		if(ele.isEmpty())
		{
			Assert.assertTrue(false, "Site should available");
		}
		else
			for(WebElement site:ele)
			{
				Thread.sleep(2000);
				site.click();
				Thread.sleep(5000);
				test.log(Status.INFO, "Selected site displayed     :"+test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Site"+i)));
				allSitesDropMenu.click();
				i++;
			}
	
	}
	public void closeSearchTextBox()
	{
		wait.forElementClickable(closeSearchIcon);
		closeSearchIcon.click();
		test.log(Status.INFO, "search box closed");
	}
	public static Date retrieveTheUpdatedDate() throws ParseException, IOException
	{
		SimpleDateFormat format =  new SimpleDateFormat("dd/MM/yyyy, mm:ss");
		wait.forElementVisible(retrieveTheDate);
		String getDate = retrieveTheDate.getText();
		//String dateNew[] = getDate.split(",");
		//String time = dateNew[0];
		Date date = format.parse(getDate);
		test.log(Status.INFO, "The current Date and Time is :" +test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "mytest")));		
		System.out.println(date);
		return date;
				
	}
	public static void clickOnRefresh()
	{
		wait.forElementClickable(clickOnRefresh);
		clickOnRefresh.click();
	}

}



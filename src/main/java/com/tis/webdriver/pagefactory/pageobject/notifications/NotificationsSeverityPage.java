package com.tis.webdriver.pagefactory.pageobject.notifications;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;
public class NotificationsSeverityPage extends TisBasePageObject
{

	public NotificationsSeverityPage(WebDriver driver) {
		super();
		
	}

	/**This method verifies the total notifications severity in list with the Severity Available in Page
	 * @throws IOException 
	 * */
	public static void verifiySeverity(String severity) throws InterruptedException, IOException
	{
		
		
		if(severity=="Critical")
		{
			Thread.sleep(2000);
		//	Assert.assertEquals(NotificationsPage.severityNotificationsFromCombo(severity),Integer.parseInt(NotificationsPage.notificationSeverity().get(0)));
			Assert.assertEquals(NotificationsPage.totalActiveNotifications(),Integer.parseInt(NotificationsPage.notificationSeverity().get(0)));
			test.log(Status.PASS, "Critical Severity Matched" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "critical")));
		}
		else if (severity=="High")
		{
			
			Thread.sleep(2000);
			Assert.assertEquals(NotificationsPage.totalActiveNotifications(),Integer.parseInt(NotificationsPage.notificationSeverity().get(1)));
			//Assert.assertEquals(NotificationsPage.severityNotificationsFromCombo(severity),Integer.parseInt(NotificationsPage.notificationSeverity().get(1)));
			test.log(Status.PASS, "High Severity Matched"+test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "high")));
		}
		else if (severity=="Medium")
		{
			Thread.sleep(2000);
			Assert.assertEquals(NotificationsPage.totalActiveNotifications(),Integer.parseInt(NotificationsPage.notificationSeverity().get(2)));
			//Assert.assertEquals(NotificationsPage.severityNotificationsFromCombo(severity),Integer.parseInt(NotificationsPage.notificationSeverity().get(2)));
			test.log(Status.PASS, "Medium Severity Matched"+test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Medium")));
		}
		else if(severity=="Low")
		{
			Thread.sleep(2000);
			Assert.assertEquals(NotificationsPage.totalActiveNotifications(),Integer.parseInt(NotificationsPage.notificationSeverity().get(3)));
			//Assert.assertEquals(NotificationsPage.severityNotificationsFromCombo(severity),Integer.parseInt(NotificationsPage.notificationSeverity().get(3)));
			test.log(Status.PASS, "Low Severity Matched"+test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "low")));
		}
		
	}
	public static void verifiySeverityInCompletedTab(String severity) throws InterruptedException, IOException
	{
		
		
		if(severity=="Critical")
		{
			Thread.sleep(2000); 
			
			Assert.assertEquals(NotificationsPage.totalCompletedNotifications(),Integer.parseInt(NotificationsPage.notificationSeverity().get(0)));
			//Assert.assertEquals(NotificationsPage.severityNotificationsFromCombo(severity),Integer.parseInt(NotificationsPage.notificationSeverity().get(0)));
			test.log(Status.PASS, "Critical Severity Matched" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "critical")));
		}
		
		else if (severity=="High")
		{
			Thread.sleep(2000);
			Assert.assertEquals(NotificationsPage.totalCompletedNotifications(),Integer.parseInt(NotificationsPage.notificationSeverity().get(1)));
			//Assert.assertEquals(NotificationsPage.severityNotificationsFromCombo(severity),Integer.parseInt(NotificationsPage.notificationSeverity().get(1)));
			test.log(Status.PASS, "High Severity Matched"+test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "high")));
		}
		else if (severity=="Medium")
			
		{
			Thread.sleep(2000);
			//Assert.assertEquals(NotificationsPage.severityNotificationsFromCombo(severity),Integer.parseInt(NotificationsPage.notificationSeverity().get(2)));
			Assert.assertEquals(NotificationsPage.totalCompletedNotifications(),Integer.parseInt(NotificationsPage.notificationSeverity().get(2)));
			test.log(Status.PASS, "Medium Severity Matched"+test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "Medium")));
		}
		else if(severity=="Low")
			
		{
			Thread.sleep(2000);
			Assert.assertEquals(NotificationsPage.totalCompletedNotifications(),Integer.parseInt(NotificationsPage.notificationSeverity().get(3)));
			//Assert.assertEquals(NotificationsPage.severityNotificationsFromCombo(severity),Integer.parseInt(NotificationsPage.notificationSeverity().get(3)));
			test.log(Status.PASS, "Low Severity Matched"+test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "low")));
		}
		
	}
	
	
	

}

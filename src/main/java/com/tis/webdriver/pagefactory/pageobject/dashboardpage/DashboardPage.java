package com.tis.webdriver.pagefactory.pageobject.dashboardpage;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.tis.webdriver.common.template.ExtentManager;
import com.tis.webdriver.pagefactory.pageobject.TisBasePageObject;

public class DashboardPage extends TisBasePageObject
{
	@FindBy (xpath =".//*[@id='spanSectionId']")
	static WebElement pageHeaderElement;
	@FindBy (xpath = "//div[@class='card-title text-uppercase txt-overflow dashboard-card-title' and @title='TASKS TO DO']")
	static WebElement linktasksToDoPopUP;
	@FindBy (xpath = "//div[@class='floating-card-title txt-overflow'and @title = 'TASKS TO DO']")
	static WebElement tasksToDoPopUP;
	@FindBy (xpath="//i[@class='glyphs btn-close-floating-card']")
	static WebElement closePopUp;
	@FindBy (xpath = "//i[@class='glyphs float-right']")
	static WebElement closeAvailableDriversPopUp;
	@FindBy (xpath = "//div[@class='card-title txt-overflow text-uppercase dashboard-card-title' and @title='AVAILABLE DRIVERS']")
	static WebElement clickOnAvailableDriversPopUp;
	
	@FindBy(xpath = "//div[@class='floating-card-title']")
	static WebElement VerifyAvaialbleDriversPopUp;
	@FindBy(css = "button[class='btn btn-primary blue-button-2 ml-1 txt-overflow']")
	static WebElement clickOnGoToNotification;
	@FindBy (xpath = "//span[@id='spanSectionId'  and contains(text(),'Notifications')]")
	static WebElement notificationsHeader;
	@FindBy (css="button[title ='GO TO DRIVERS']")
	static WebElement bttnGoToDriver;
	@FindBy (xpath = "//span[@id='spanSectionId' and contains(text(),'Drivers')]")
	static WebElement driverHeadertxt;
	
	
	public static void  verifyPageHeader() throws IOException, InterruptedException
	{
		Thread.sleep(7000);
		wait.forElementVisible(pageHeaderElement);
		System.out.println(pageHeaderElement.getText());
		if(pageHeaderElement.getText().contains("DASHBOARD"))
		{
			test.log(Status.PASS, "Dashboard Page displayed" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, "header")));
		}
		else
		{
			Assert.assertTrue(false);
		}
		
	}

	public static void clickOnTaskToDoPupUp() 
	{
		wait.forElementClickable(linktasksToDoPopUP);
		linktasksToDoPopUP.click();
	
	}
	public static void verifyTasksToDoPopUp() throws IOException
	{
		wait.forElementVisible(tasksToDoPopUP);
		test.log(Status.PASS, "TasksToDo pop up displayed" +test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver,"taskstodoimg")));
		
	}

	public static void closePopUp()
	{
		wait.forElementClickable(closePopUp);
		closePopUp.click();
	}
	
	public static void clickOnAvailableDriversPopUp()
	{
		wait.forElementClickable(clickOnAvailableDriversPopUp);
		clickOnAvailableDriversPopUp.click();
		
	}

	public static void verifyAvailableDriverPopUp() throws IOException
	{
		wait.forElementVisible(VerifyAvaialbleDriversPopUp);
		test.log(Status.PASS, "Available Drivers pop up displayed" +test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver,"available drivers")));
		
	}
	public static void closeAvailableDriversPopUp()
	{
		wait.forElementClickable(closeAvailableDriversPopUp);
		closeAvailableDriversPopUp.click();
	}

	public static void clickOnGoToNotification()
	{
		wait.forElementVisible(clickOnGoToNotification);
		clickOnGoToNotification.click();
		test.log(Status.INFO, "Notifications Button Clicked");
		
	}
	public static void verifyNotificationsPage() throws IOException
	{
		wait.forElementVisible(notificationsHeader);
		test.log(Status.PASS, "Notifications Page Displayed" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver,"available drivers")));
	}
	public static void clickOnGoToDrivers()
	{
		wait.forElementClickable(bttnGoToDriver);
		bttnGoToDriver.click();
	}
	public static void verifyDriversPage() throws IOException
	{
		wait.forElementVisible(driverHeadertxt);
		test.log(Status.PASS, "Notifications Page Displayed" + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver,"driverheader")));
	}
	

}

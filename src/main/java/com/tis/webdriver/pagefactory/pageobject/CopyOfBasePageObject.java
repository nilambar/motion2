package com.tis.webdriver.pagefactory.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;



import com.tis.webdriver.common.template.ExtentManager;

public class CopyOfBasePageObject extends ExtentManager
{
	 protected static WebDriver driver;
	
	public CopyOfBasePageObject(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
		
	}
	
	protected void getURL(String url)
	{
		getURL(url,true);
	}

	private void getURL(String url, boolean urlPresent)
	{
		driver.get(url);
		/*if(urlPresent)
		{
			Reporter.log("Url present");
		}*/
	}

}

package com.tis.webdriver.common.template;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;






import com.aventstack.extentreports.Status;

public class NewTestTemplate  extends ExtentManager
{
	
	
	//public static String tisURL="http://continental.atos.motion2.s3-website.ap-south-1.amazonaws.com/";
	//public static String tisURL="http://demo-motion2-20170619.s3-website.ap-south-1.amazonaws.com";
	//public static String tisURL = "http://www.google.co.in";
	
	protected static WebDriver driver;
	
	//@SuppressWarnings({ "rawtypes", "unchecked" })
	@BeforeMethod(alwaysRun = true)
	@org.testng.annotations.Parameters(value={"config", "environment"})
	public void setUp(String config_file, String environment)throws Exception
	{
	
			
	        JSONParser parser = new JSONParser();
	        JSONObject config = (JSONObject) parser.parse(new FileReader("C:\\NewRepository\\TIS_NextGen\\src\\main\\java\\com\\tis\\webdriver\\common\\configuration\\" + config_file));
	        JSONObject envs = (JSONObject) config.get("environments");

	        DesiredCapabilities capabilities = new DesiredCapabilities();

	        Map<String, String> envCapabilities = (Map<String, String>) envs.get(environment);
	        Iterator it = envCapabilities.entrySet().iterator();
	        while (it.hasNext()) {
	            Map.Entry pair = (Map.Entry)it.next();
	            capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
	        }
	        String username = System.getenv("BROWSERSTACK_USERNAME");
	        if(username == null) {
	            username = (String) config.get("user");
	        }

	        String accessKey = System.getenv("BROWSERSTACK_ACCESS_KEY");
	        if(accessKey == null) {
	            accessKey = (String) config.get("key");
	        }
       driver = DriverProvider.getDriver(environment);
	   ExtentManager.GetExtent();
	        		
    }
	
	@AfterMethod(alwaysRun = true)
	public void getResult(ITestResult result) throws IOException
	{
		if(result.getStatus() == ITestResult.FAILURE)
		{
			test.log(Status.FAIL, "Test Case Failed is "+result.getName());
			test.log(Status.FAIL, "Test Case Failed is "+result.getThrowable());
			test.log(Status.FAIL,  "Fail: " + test.addScreenCaptureFromPath(ExtentManager.takeScreenShot(driver, result.getTestName())));
		}else if(result.getStatus() == ITestResult.SKIP)
		{
			test.log(Status.SKIP, "Test Case Skipped is "+result.getName());
		}
		else if(result.getStatus() == ITestResult.SUCCESS)
		{
			test.log(Status.PASS, "Pass: "+result.getName());
		}
		
	}
	
	@AfterSuite(alwaysRun = true)
	public void tearDown(){
		extent.flush();
		driver.quit();
		
	}

}

package com.tis.webdriver.common.template;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;


public class DriverProvider
{
	public static WebDriver  driver = null;
	//static String URL= "http://demo-motion2-20170619.s3-website.ap-south-1.amazonaws.com";
	//static String URL= "http://continental.atos.motion2.s3-website.ap-south-1.amazonaws.com";
	//static String URL = "http://continental.atos.motion2.s3-website.ap-south-1.amazonaws.com/notifications/active";
	//static String URL= "http://continental.atos.motion2.s3-website.ap-south-1.amazonaws.com";
	//static String URL ="http://13.126.68.211/FleetServices/";
	static String URL ="https://www.at.my-fis.com/MotionNextGen/";
	public static Process pr;
	
	public static  WebDriver getDriver(String browser) throws InterruptedException
	{
		if(driver ==null)
		{
			if(browser.equalsIgnoreCase("firefox"))
			{
				System.setProperty("webdriver.gecko.driver", "./InputTestData/geckodriver.exe");
				
				DesiredCapabilities capabilities = DesiredCapabilities.firefox();
	        	capabilities.setJavascriptEnabled(true); 
	        	driver=new FirefoxDriver(capabilities); 
	        	driver.manage().window().maximize();
	        	driver.get(URL);
			}
			else if(browser.equalsIgnoreCase("chrome"))
			{
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	        	capabilities.setJavascriptEnabled(true); 
				System.setProperty("webdriver.chrome.driver", "./InputTestData/chromedriver.exe");
				driver=new ChromeDriver(capabilities);
				driver.manage().window().maximize();
				driver.get(URL);
				//pr.destroy();
				
			}
			else if(browser.equalsIgnoreCase("ie"))
			{
				
				System.setProperty("webdriver.ie.driver","C:\\Selenium\\IEdriver\\IEDriverServer_x64_3.1.0\\IEDriverServer.exe");
				DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setCapability(CapabilityType.BROWSER_NAME, "IE");
				capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
				capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
				capabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, URL);
				driver.manage().window().maximize();
				driver.get(URL);
				//driver = new InternetExplorerDriver(capabilities);
			}
				
		}
		//driver.manage().window().maximize();
		Thread.sleep(3000);
		//driver.get(URL);
		return driver;
	}

}

package com.tis.webdriver.common.template;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;


import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.tis.webdriver.common.core.element.Wait;

public class ExtentManager 
{

	protected static ExtentReports extent;
	protected static ExtentTest test;
	private static ExtentHtmlReporter htmlReporter;
	//private static String filePath = "./extentreport.html";
	
	public static ExtentReports GetExtent() throws Exception{
		if (extent != null)
                    return extent; //avoid creating new instance of html file
                extent = new ExtentReports();		
		extent.attachReporter(getHtmlReporter());
		return extent;
	}
	
 
	private static ExtentHtmlReporter getHtmlReporter() throws Exception {
	
        htmlReporter = new ExtentHtmlReporter(ExtentManager.filePath());
		
	// make the charts visible on report open
        htmlReporter.config().setChartVisibilityOnOpen(true);
		
        htmlReporter.config().setDocumentTitle("TIS Automation Report");
        htmlReporter.config().setReportName("Regression cycle");
        return htmlReporter;
	}
	
	public static ExtentTest createTest(String name, String description){
		test = extent.createTest(name, description);
		return test;
	}
	public static String filePath() throws Exception
	{
		//String filePath="./Results/"+ExtentManager.createTimeStampStr()+"SummaryReport.html";
		String filePath="../Results/"+ExtentManager.createTimeStampStr()+"SummaryReport.html";
		return filePath;
	}
	public static String createTimeStampStr() throws Exception {
		Calendar mycalendar = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_hhmmss");
		String timeStamp = formatter.format(mycalendar.getTime());

		return timeStamp;

	}
	
	public static String captureAndDisplayScreenShot(WebDriver driver, ExtentTest test){
		 String extentReportImage = "../extentReport/screenshots/" + System.currentTimeMillis() + ".png";
		// Take screenshot and store as a file format
		File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			// now copy the screenshot to desired location using copyFile method
			FileUtils.copyFile(src, new File(extentReportImage));
			test.log(Status.INFO, "Screenshot from : " + extentReportImage, MediaEntityBuilder.createScreenCaptureFromPath(extentReportImage).build());
		} catch (IOException e)
		{
			System.out.println("Error in the captureAndDisplayScreenShot method: " + e.getMessage());
		}
		return extentReportImage;
	}
	

	public static String takeScreenShot(WebDriver driver, String methodName) {
	        String path = "../Results/TisScreenShots/" + methodName+System.currentTimeMillis() +".png";
	        try {
	            File screenshotFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	            FileUtils.copyFile(screenshotFile, new File(path));
	        } catch (Exception e) {
	           System.out.println(("Could not write screenshot" + e));
	           
	        }
	        return path;
	    }

}
